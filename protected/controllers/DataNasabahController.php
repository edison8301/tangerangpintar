<?php

class DataNasabahController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/column2';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','exportExcel','cair','usulan','import','importUsulan'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','persyaratanLengkap','dataCair'),
				'users'=>array('@'),
			),	
			array('deny',  // deny all users
				'users'=>array('*'),
			),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new DataNasabah;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DataNasabah']))
		{
			$model->attributes=$_POST['DataNasabah'];
			$model->id_sekolah = Yii::app()->user->getState('id_sekolah');
			
			if($model->save())
			{
			//$this->redirect(array('view','id'=>$model->id));
			Yii::app()->user->setFlash('success','Data berhasil disimpan');
			$this->redirect(array('view','id'=>$model->id));
			}
		}
				

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionImport()
	{
		$model = new Import;
		
		if(isset($_POST['Import']))
		{
			$model->attributes = $_POST['Import'];
			
			$file = CUploadedFile::getInstance($model,'file');
			
			if($file!==null)
			{
				$path = Yii::app()->basePath.'/../imports/';
				if($file->saveAs($path.$file->name)) {
					spl_autoload_unregister(array('YiiBase','autoload'));
					Yii::import('application.extensions.PHPExcel',true);
					spl_autoload_register(array('YiiBase', 'autoload'));

					$filename = $path.$file->name;
		
					$objReader = PHPExcel_IOFactory::createReader("Excel2007");
					$objPHPExcel = $objReader->load($filename);
		
					$highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		
					for($i=8;$i<=$highestRow;$i++)
					{
						$nama = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue();
						$ttl = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue();
						$nis = $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue();
						$nisn = $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue();
						$nama_bank = $objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue();
						$no_rek = $objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue();
						$jk = $objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue();
						$kelas = $objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue();
						$jurusan = $objPHPExcel->getActiveSheet()->getCell('K'.$i)->getValue();
						$nama_bapak = $objPHPExcel->getActiveSheet()->getCell('L'.$i)->getValue();
						$nama_ibu = $objPHPExcel->getActiveSheet()->getCell('M'.$i)->getValue();
						$alamat = $objPHPExcel->getActiveSheet()->getCell('N'.$i)->getValue();
						$id_kriteria = $objPHPExcel->getActiveSheet()->getCell('O'.$i)->getValue();
						$asal_sekolah = $objPHPExcel->getActiveSheet()->getCell('P'.$i)->getValue();
						$jumlah_diterima = $objPHPExcel->getActiveSheet()->getCell('Q'.$i)->getValue();
			
						$explode = explode(', ',$ttl);
						if(!empty($ttl))
							$tempat_lahir = trim($explode[0]); 
						else
							$tempat_lahir = '-';
						if(!empty($ttl)) {
							$tanggal = explode('-',$explode[1]);
							$tanggal_lahir = trim($tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0]); 
						} else {
							$tanggal_lahir = '-';
						}
						if($jk=='L')
							$jenis_kelamin = JenisKelamin::model()->findByPk(1)->id;
						else if($jk=='P')
							$jenis_kelamin = JenisKelamin::model()->findByPk(2)->id;
						/*if(empty($nis))
							$nis = '-';
						if(empty($nisn))
							$nisn = '-';
						if(empty($nama_bank))
							$nama_bank = '-';
						if(empty($no_rek))
							$no_rek = '-';
						if(empty($kelas))
							$kelas = '-';
						if(empty($jurusan))
							$jurusan = '-';
						if(empty($nama_bapak))
							$nama_bapak = '-';
						if(empty($nama_ibu))
							$nama_ibu = '-';
						if(empty($alamat))
							$alamat = '-';
						if(empty($id_kriteria))
							$id_kriteria = 0;
						if(empty($jumlah_diterima))
							$jumlah_diterima = '-';*/
						$sekolah = Sekolah::model()->findByAttributes(array('nama_sekolah'=>$asal_sekolah));
						if($sekolah!=null)
							$id_sekolah = $sekolah->id; 
						else {
							$sekolah = new Sekolah;
							$sekolah->nama_sekolah = $asal_sekolah;
							$sekolah->jenjang_sekolah_id = 3;
							$sekolah->email = '-';
							$sekolah->alamat = '-';
							$sekolah->rt = '-';
							$sekolah->rw = '-';
							$sekolah->kelurahan = '-';
							$sekolah->kecamatan = '-';
							$sekolah->kota = '-';
							$sekolah->provinsi = '-';
							$sekolah->telepon = '-';
							$sekolah->kode_pos = '-';
							$sekolah->save();
							$id_sekolah = $sekolah->id;
						}
						$model = new DataNasabah;
						$model->nama = $nama;
						$model->tempat_lahir = $tempat_lahir;
						$model->tanggal_lahir = $tanggal_lahir;
						$model->no_identitas = $nis;
						$model->nisn = $nisn;
						$model->bank = $nama_bank;
						$model->no_rekening = $no_rek;
						$model->id_jk = $jenis_kelamin;
						$model->kelas = $kelas;
						$model->jurusan = $jurusan;
						$model->nama_bapak = $nama_bapak;
						$model->nama_ibu = $nama_ibu;
						$model->alamat = $alamat;
						$model->id_kriteria = $id_kriteria;
						$model->id_sekolah = $id_sekolah;
						$model->jumlah_nominal = $jumlah_diterima;
						$model->id_kebangsaan = 1;
						$model->tahun = 2015;
						$model->cair = 0;
						$model->persyaratan = 0;
						$model->save();
						Yii::app()->user->setFlash('success','Data Nasabah Berhasil Di Import');
					}
				}
			}
		}
		
		$this->render('import',array(
			'model'=>$model
		));
	}
	
	public function actionImportUsulan()
	{
		$model = new Import;
		
		if(isset($_POST['Import']))
		{
			$model->attributes = $_POST['Import'];
			
			$file = CUploadedFile::getInstance($model,'file');
			
			if($file!==null)
			{
				$path = Yii::app()->basePath.'/../imports/';
				if($file->saveAs($path.$file->name)) {
					spl_autoload_unregister(array('YiiBase','autoload'));
					Yii::import('application.extensions.PHPExcel',true);
					spl_autoload_register(array('YiiBase', 'autoload'));

					$filename = $path.$file->name;
		
					$objReader = PHPExcel_IOFactory::createReader("Excel2007");
					$objPHPExcel = $objReader->load($filename);
		
					$highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		
					for($i=9;$i<=$highestRow;$i++)
					{
						$nama = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue();
						$tempat_lahir = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue();
						$tgl = $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue();
						$bulan = $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue();
						$tahun = $objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue();
						$nis = $objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue();
						$nisn = $objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue();
						$jk = $objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue();
						$kelas = $objPHPExcel->getActiveSheet()->getCell('K'.$i)->getValue();
						$jurusan = $objPHPExcel->getActiveSheet()->getCell('M'.$i)->getValue();
						$nama_bapak = $objPHPExcel->getActiveSheet()->getCell('N'.$i)->getValue();
						$nama_ibu = $objPHPExcel->getActiveSheet()->getCell('O'.$i)->getValue();
						$alamat = $objPHPExcel->getActiveSheet()->getCell('P'.$i)->getValue();
						$id_kriteria = $objPHPExcel->getActiveSheet()->getCell('Q'.$i)->getValue();
						$asal_sekolah = $objPHPExcel->getActiveSheet()->getCell('R'.$i)->getValue();
			
						
						$tanggal_lahir = trim($tahun.'-'.$bulan.'-'.$tgl); 
						if($jk=='L')
							$jenis_kelamin = JenisKelamin::model()->findByPk(1)->id;
						else if($jk=='P')
							$jenis_kelamin = JenisKelamin::model()->findByPk(2)->id;
						/*if(empty($nis))
							$nis = '-';
						if(empty($nisn))
							$nisn = '-';
						if(empty($nama_bank))
							$nama_bank = '-';
						if(empty($no_rek))
							$no_rek = '-';
						if(empty($kelas))
							$kelas = '-';
						if(empty($jurusan))
							$jurusan = '-';
						if(empty($nama_bapak))
							$nama_bapak = '-';
						if(empty($nama_ibu))
							$nama_ibu = '-';
						if(empty($alamat))
							$alamat = '-';
						if(empty($id_kriteria))
							$id_kriteria = 0;
						if(empty($jumlah_diterima))
							$jumlah_diterima = '-';*/
						$sekolah = Sekolah::model()->findByAttributes(array('nama_sekolah'=>$asal_sekolah));
						if($sekolah!=null)
							$id_sekolah = $sekolah->id; 
						else {
							$sekolah = new Sekolah;
							$sekolah->nama_sekolah = $asal_sekolah;
							$sekolah->jenjang_sekolah_id = 3;
							$sekolah->email = '-';
							$sekolah->alamat = '-';
							$sekolah->rt = '-';
							$sekolah->rw = '-';
							$sekolah->kelurahan = '-';
							$sekolah->kecamatan = '-';
							$sekolah->kota = '-';
							$sekolah->provinsi = '-';
							$sekolah->telepon = '-';
							$sekolah->kode_pos = '-';
							$sekolah->save();
							$id_sekolah = $sekolah->id;
						}
						$model = new DataNasabah;
						$model->nama = $nama;
						$model->tempat_lahir = $tempat_lahir;
						$model->tanggal_lahir = $tanggal_lahir;
						$model->no_identitas = $nis;
						$model->nisn = $nisn;
						$model->id_jk = $jenis_kelamin;
						$model->kelas = $kelas;
						$model->jurusan = $jurusan;
						$model->nama_bapak = $nama_bapak;
						$model->nama_ibu = $nama_ibu;
						$model->alamat = $alamat;
						$model->id_kriteria = $id_kriteria;
						$model->id_sekolah = $id_sekolah;
						$model->id_kebangsaan = 1;
						$model->tahun = 2015;
						$model->cair = 0;
						$model->persyaratan = 0;
						$model->save();
						Yii::app()->user->setFlash('success','Data Nasabah Berhasil Di Import');
					}
				}
			}
		}
		
		$this->render('import',array(
			'model'=>$model
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['DataNasabah']))
{
$model->attributes=$_POST['DataNasabah'];
if($model->save())
{
//$this->redirect(array('view','id'=>$model->id));
Yii::app()->user->setFlash('success','Data berhasil disimpan');
$this->redirect(array('view','id'=>$model->id));
}
}

$this->render('update',array(
'model'=>$model,
));
}

	public function actionPersyaratanLengkap($id)
	{
		$model=$this->loadModel($id);
        $model->persyaratan = new CDbExpression('1'); 
        if($model->save(false))
        {
			Yii::app()->user->setFlash('success','Data berhasil disimpan');
			$this->redirect(array('admin'));
        }
	}

	public function actionDataCair($id)
	{
		$model=$this->loadModel($id);
        $model->cair = new CDbExpression('1'); 
        
		if($model->save(false))
        {
			Yii::app()->user->setFlash('success','Data nasabah terkait berhasil dicairkan');
            $this->redirect(array('admin'));
        }
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('DataNasabah');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new DataNasabah('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DataNasabah']))
			$model->attributes=$_GET['DataNasabah'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionUsulan()
	{
		$model=new DataNasabah('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['DataNasabah']))
			$model->attributes=$_GET['DataNasabah'];

		$this->render('usulan',array(
			'model'=>$model,
		));
	}
	
	public function actionCair()
	{
		$model=new DataNasabah('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['DataNasabah']))
			$model->attributes=$_GET['DataNasabah'];

		$this->render('cair',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=DataNasabah::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='data-nasabah-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
	public function actionExportExcel() 
	{
	     // get a reference to the path of PHPExcel classes 
		$phpExcelPath = Yii::getPathOfAlias('ext');
	 
		// Turn off our amazing library autoload 
		spl_autoload_unregister(array('YiiBase','autoload'));        
	 
		//
		// making use of our reference, include the main class
		// when we do this, phpExcel has its own autoload registration
		// procedure (PHPExcel_Autoloader::Register();)
		include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
	    
		spl_autoload_register(array('YiiBase','autoload'));        
		
	    $objPHPExcel = new PHPExcel();
		$styleArray = array(
			'font' => array(
			'bold' => true,
		),
			'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		),
			'borders' => array(
			'bottom' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
		),
	),
			'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
			'rotation' => 90,
			'startcolor' => array(
			'argb' => 'FFA0A0A0',
		),
			'endcolor' => array(
			'argb' => 'FFFFFFFF',
		),
	),
);

	    $objPHPExcel->getActiveSheet()->setTitle('Data Nasabah')->getStyle('A1:BB1')->applyFromArray($styleArray);;
	 
	    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(25);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth(22);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setWidth(22);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setWidth(22);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setWidth(20);

	    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'No');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Nama');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Jenis Kelamin');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Kebangsaan');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Tempat Lahir');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Tanggal Lahir');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'No Identitas');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Masa Berlaku Identitas');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'NPWP');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Nama Ibu');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Status Kawin');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Agama');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Pendidikan');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Alamat');
		$objPHPExcel->getActiveSheet()->setCellValue('O1', 'RT');
		$objPHPExcel->getActiveSheet()->setCellValue('P1', 'RW');
		$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Kelurahan');
		$objPHPExcel->getActiveSheet()->setCellValue('R1', 'Kecamatan');
		$objPHPExcel->getActiveSheet()->setCellValue('S1', 'Kota');
		$objPHPExcel->getActiveSheet()->setCellValue('T1', 'Propinsi');
		$objPHPExcel->getActiveSheet()->setCellValue('U1', 'Kode Pos');
		$objPHPExcel->getActiveSheet()->setCellValue('V1', 'Status Tempat Tinggal');
		$objPHPExcel->getActiveSheet()->setCellValue('W1', 'No HP');
		$objPHPExcel->getActiveSheet()->setCellValue('X1', 'No Telp Rumah');
		$objPHPExcel->getActiveSheet()->setCellValue('Y1', 'Alamat Surat');
		$objPHPExcel->getActiveSheet()->setCellValue('Z1', 'Tipe Alamat');
		$objPHPExcel->getActiveSheet()->setCellValue('AA1', 'Pekerjaan');
		$objPHPExcel->getActiveSheet()->setCellValue('AB1', 'Kode Profesi');
		$objPHPExcel->getActiveSheet()->setCellValue('AC1', 'Status Pekerjaan');
		$objPHPExcel->getActiveSheet()->setCellValue('AD1', 'Nama Instansi');
		$objPHPExcel->getActiveSheet()->setCellValue('AE1', 'Alamat Instansi');
		$objPHPExcel->getActiveSheet()->setCellValue('AF1', 'RT');
		$objPHPExcel->getActiveSheet()->setCellValue('AG1', 'RW');
		$objPHPExcel->getActiveSheet()->setCellValue('AH1', 'Kelurahan');
		$objPHPExcel->getActiveSheet()->setCellValue('AI1', 'Kecamatan');
		$objPHPExcel->getActiveSheet()->setCellValue('AJ1', 'Kota');
		$objPHPExcel->getActiveSheet()->setCellValue('AK1', 'Propinsi');
		$objPHPExcel->getActiveSheet()->setCellValue('AL1', 'Kode Pos');
		$objPHPExcel->getActiveSheet()->setCellValue('AM1', 'No Telp');
		$objPHPExcel->getActiveSheet()->setCellValue('AN1', 'Suami/Istri');
		$objPHPExcel->getActiveSheet()->setCellValue('AO1', 'Nama');
		$objPHPExcel->getActiveSheet()->setCellValue('AP1', 'Hubungan');
		$objPHPExcel->getActiveSheet()->setCellValue('AQ1', 'Alamat');
		$objPHPExcel->getActiveSheet()->setCellValue('AR1', 'Kota');
		$objPHPExcel->getActiveSheet()->setCellValue('AS1', 'Propinsi');
		$objPHPExcel->getActiveSheet()->setCellValue('AT1', 'No Telp');
		$objPHPExcel->getActiveSheet()->setCellValue('AU1', 'Sumber Dana');
		$objPHPExcel->getActiveSheet()->setCellValue('AV1', 'Tujuan Penggunaan Dana');
		$objPHPExcel->getActiveSheet()->setCellValue('AW1', 'Penghasilan Per Tahun');
		$objPHPExcel->getActiveSheet()->setCellValue('AX1', 'Penghasilan Tambahan Per Tahun');
		$objPHPExcel->getActiveSheet()->setCellValue('AY1', 'Max Setoran Per Bulan');
		$objPHPExcel->getActiveSheet()->setCellValue('AZ1', 'Max Tarikan Per Bulan');
		$objPHPExcel->getActiveSheet()->setCellValue('BA1', 'KJP');
		$objPHPExcel->getActiveSheet()->setCellValue('BB1', 'NISN');
	    
		
		$i=2; $no=1;
		
		$criteria = new CDbCriteria;
		
		if(isset($_GET['persyaratan']))
		{
			$criteria->condition = 'persyaratan = :persyaratan';
			$criteria->params = array(':persyaratan'=>$_GET['persyaratan']);
		}
		
		if(isset($_GET['cair']))
		{
			$criteria->condition = 'cair = :cair';
			$criteria->params = array(':cair'=>$_GET['cair']);
		}
		
		foreach(DataNasabah::model()->findAll($criteria) as $data){
		$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A'.$i.'', $no)
		    ->setCellValue('B'.$i.'', $data->nama)
		    ->setCellValue('C'.$i.'', $data->getRelationField("Jk","jenis_kelamin"))
		    ->setCellValue('D'.$i.'', $data->getRelationField("Kebangsaan","kebangsaan"))
		    ->setCellValue('E'.$i.'', $data->tempat_lahir)
		    ->setCellValue('F'.$i.'', Yii::app()->dateFormatter->format("yyyyMMdd",$data->tanggal_lahir))
			->setCellValue('G'.$i.'', $data->no_identitas)
			->setCellValue('H'.$i.'', $data->mb_identitas)
			->setCellValue('I'.$i.'', $data->npwp)
			->setCellValue('J'.$i.'', $data->nama_ibu)
			->setCellValue('K'.$i.'', $data->getRelationField("StatusKawin","status_kawin"))
			->setCellValue('L'.$i.'', $data->getRelationField("Agama","agama"))
			->setCellValue('M'.$i.'', $data->getRelationField("Pendidikan","pendidikan"))
			->setCellValue('N'.$i.'', $data->alamat)
			->setCellValue('O'.$i.'', $data->rt)
			->setCellValue('P'.$i.'', $data->rw)
			->setCellValue('Q'.$i.'', $data->kelurahan)
			->setCellValue('R'.$i.'', $data->kecamatan)
			->setCellValue('S'.$i.'', $data->kota)
			->setCellValue('T'.$i.'', $data->propinsi)
			->setCellValue('U'.$i.'', $data->kode_pos)
			->setCellValue('V'.$i.'', $data->getRelationField("Stt","status_ttinggal"))
			->setCellValue('W'.$i.'', $data->no_hp)
			->setCellValue('X'.$i.'', $data->no_rumah)
			->setCellValue('Y'.$i.'', $data->getRelationField("AlamatSurat","alamat_surat"))
			->setCellValue('Z'.$i.'', $data->getRelationField("TipeAlamat","tipe_alamat"))
			->setCellValue('AA'.$i.'', $data->getRelationField("Pekerjaan","pekerjaan"))
			->setCellValue('AB'.$i.'', $data->getRelationField("KodeProfesi","kode_profesi"))
			->setCellValue('AC'.$i.'', $data->getRelationField("StatusPekerjaan","status_pekerjaan"))
			->setCellValue('AD'.$i.'', $data->nama_instansi)
			->setCellValue('AE'.$i.'', $data->alamat_instansi)
			->setCellValue('AF'.$i.'', $data->rt_dp)
			->setCellValue('AG'.$i.'', $data->rw_dp)
			->setCellValue('AH'.$i.'', $data->kelurahan_dp)
			->setCellValue('AI'.$i.'', $data->kecamatan_dp)
			->setCellValue('AJ'.$i.'', $data->kota_dp)
			->setCellValue('AK'.$i.'', $data->propinsi_dp)
			->setCellValue('AL'.$i.'', $data->kodepos_dp)
			->setCellValue('AM'.$i.'', $data->notelp_dp)
			->setCellValue('AN'.$i.'', $data->suami_istri)
			->setCellValue('AO'.$i.'', $data->nama_pydd)
			->setCellValue('AP'.$i.'', $data->getRelationField("Hubungan","hubungan"))
			->setCellValue('AQ'.$i.'', $data->alamat_pydd)
			->setCellValue('AR'.$i.'', $data->kota_pydd)
			->setCellValue('AS'.$i.'', $data->propinsi_pydd)
			->setCellValue('AT'.$i.'', $data->notelp_pydd)
			->setCellValue('AU'.$i.'', $data->getRelationField("SumberDana","sumber_dana"))
			->setCellValue('AV'.$i.'', $data->getRelationField("Tpd","tujuan_pdana"))
			->setCellValue('AW'.$i.'', $data->getRelationField("Ppt","pp_tahun"))
			->setCellValue('AX'.$i.'', $data->getRelationField("Ptp","ptp_tahun"))
			->setCellValue('AY'.$i.'', $data->getRelationField("Maxspb","max_spbulan"))
			->setCellValue('AZ'.$i.'', $data->getRelationField("Maxtpb","max_tpbulan"))
			->setCellValue('BA'.$i.'', $data->kjp)
			->setCellValue('BB'.$i.'', $data->nisn);
			$i++;$no++;
		}
	    ob_end_clean();
	    ob_start();
	    
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Export_Data_Nasabah.xls"');
	    header('Cache-Control: max-age=0');
	   
	   $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	   $objWriter->save('php://output');
  	}
}