<?php

/**
 * This is the model class for table "sekolah".
 *
 * The followings are the available columns in table 'sekolah':
 * @property integer $id
 * @property string $nama_sekolah
 * @property integer $jenjang_sekolah_id
 * @property string $email
 * @property string $alamat
 * @property string $rt
 * @property string $rw
 * @property string $kelurahan
 * @property string $kecamatan
 * @property string $kota
 * @property string $provinsi
 * @property string $kode_pos
 * @property string $telepon
 */
class Sekolah extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sekolah';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_sekolah, jenjang_sekolah_id, email, alamat, rt, rw, kelurahan, kecamatan, kota, provinsi, kode_pos, telepon', 'required'),
			array('jenjang_sekolah_id', 'numerical', 'integerOnly'=>true),
			array('nama_sekolah, alamat', 'length', 'max'=>255),
			array('email, kelurahan, kecamatan, kota, provinsi, telepon', 'length', 'max'=>50),
			array('rt, rw, kode_pos', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama_sekolah, jenjang_sekolah_id, email, alamat, rt, rw, kelurahan, kecamatan, kota, provinsi, kode_pos, telepon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'Sekolah'=>array(self::BELONGS_TO,'JenjangSekolah','jenjang_sekolah_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_sekolah' => 'Nama Sekolah',
			'jenjang_sekolah_id' => 'Jenjang Sekolah',
			'email' => 'Email',
			'alamat' => 'Alamat',
			'rt' => 'RT',
			'rw' => 'RW',
			'kelurahan' => 'Kelurahan',
			'kecamatan' => 'Kecamatan',
			'kota' => 'Kota',
			'provinsi' => 'Provinsi',
			'kode_pos' => 'Kode Pos',
			'telepon' => 'Telepon',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_sekolah',$this->nama_sekolah,true);
		$criteria->compare('jenjang_sekolah_id',$this->jenjang_sekolah_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('rt',$this->rt,true);
		$criteria->compare('rw',$this->rw,true);
		$criteria->compare('kelurahan',$this->kelurahan,true);
		$criteria->compare('kecamatan',$this->kecamatan,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('provinsi',$this->provinsi,true);
		$criteria->compare('kode_pos',$this->kode_pos,true);
		$criteria->compare('telepon',$this->telepon,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sekolah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
