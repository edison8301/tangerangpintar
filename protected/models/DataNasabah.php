<?php

/**
 * This is the model class for table "data_nasabah".
 *
 * The followings are the available columns in table 'data_nasabah':
 * @property integer $id
 * @property string $nama
 * @property integer $id_jk
 * @property integer $id_kebangsaan
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $no_identitas
 * @property string $mb_identitas
 * @property string $npwp
 * @property string $nama_ibu
 * @property integer $id_sk
 * @property integer $id_agama
 * @property integer $id_pendidikan
 * @property string $alamat
 * @property string $rt
 * @property string $rw
 * @property string $kelurahan
 * @property string $kecamatan
 * @property string $kota
 * @property string $propinsi
 * @property string $kode_pos
 * @property integer $id_stt
 * @property string $no_hp
 * @property string $no_rumah
 * @property integer $id_asurat
 * @property integer $id_tialamat
 * @property integer $id_pekerjaan
 * @property integer $kode_profesi
 * @property integer $status_pekerjaan
 * @property string $nama_instansi
 * @property string $alamat_instansi
 * @property string $rt_dp
 * @property string $rw_dp
 * @property string $kelurahan_dp
 * @property string $kecamatan_dp
 * @property string $kota_dp
 * @property string $propinsi_dp
 * @property string $kodepos_dp
 * @property string $notelp_dp
 * @property string $suami_istri
 * @property string $nama_pydd
 * @property integer $id_hubungan
 * @property string $alamat_pydd
 * @property string $kota_pydd
 * @property string $propinsi_pydd
 * @property string $notelp_pydd
 * @property integer $sumber_dana
 * @property integer $tp_dana
 * @property integer $pp_tahun
 * @property integer $ptp_tahun
 * @property integer $max_spbulan
 * @property integer $max_tpbulan
 * @property string $kjp
 * @property string $nisn
 */
class DataNasabah extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'data_nasabah';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, id_jk, id_kebangsaan, tempat_lahir, tanggal_lahir, 
				no_identitas, mb_identitas, npwp, nama_ibu, id_sk, id_agama, 
				id_pendidikan, alamat, rt, rw, kelurahan, kecamatan, kota, 
				propinsi, kode_pos, id_stt, no_hp, no_rumah, id_asurat, 
				id_tialamat, id_pekerjaan, kode_profesi, status_pekerjaan, 
				nama_instansi, alamat_instansi, rt_dp, rw_dp, kelurahan_dp, 
				kecamatan_dp, kota_dp, propinsi_dp, kodepos_dp, notelp_dp, 
				suami_istri, nama_pydd, id_hubungan, alamat_pydd, kota_pydd, 
				propinsi_pydd, notelp_pydd, sumber_dana, tp_dana, pp_tahun, 
				ptp_tahun, max_spbulan, max_tpbulan, kjp, nisn, no_rekening,
				nominal, jumlah_bulan, alamat_sekolah, bank, kelas, tahun', 'safe',
				'message'=>'{attribute} tidak boleh kosong.'
			),
			array('id_jk, id_kebangsaan, id_sk, id_agama, id_pendidikan, 
				id_stt, id_asurat, id_tialamat, id_pekerjaan, kode_profesi, 
				status_pekerjaan, id_hubungan, sumber_dana, tp_dana, pp_tahun, 
				ptp_tahun, max_spbulan, max_tpbulan,persyaratan,cair', 'numerical', 'integerOnly'=>true
			),
			/*array('', 'length', 'max'=>50
			),
			array('tanggal_lahir, nama, tanggal_lahir, nama_ibu, kelurahan, kecamatan, kota, propinsi, 
				nama_instansi, alamat_instansi, kelurahan_dp, kecamatan_dp, 
				kota_dp, propinsi_dp, nama_pydd', 'length', 'max'=>255),
			array('tempat_lahir, kjp', 'length', 'max'=>25),
			array('mb_identitas', 'length', 'max'=>8),
			array('no_identitas, npwp', 'length', 'max'=>16),
			array('alamat, alamat_pydd', 'length', 'max'=>75),
			array('rt, rw, rt_dp, rw_dp', 'length', 'max'=>3),
			array('kode_pos, kodepos_dp', 'length', 'max'=>5),
			array('no_hp, notelp_dp, no_rumah, notelp_pydd', 'length', 'max'=>13),
			array('suami_istri', 'length', 'max'=>15),
			array('kota_pydd, propinsi_pydd', 'length', 'max'=>30),
			array('nisn', 'length', 'max'=>10),*/
			array('kota_dp','safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_sekolah, persyaratan, tahun, nama, id_jk, id_kebangsaan, tempat_lahir, tanggal_lahir, no_identitas, mb_identitas, npwp, nama_ibu, id_sk, id_agama, id_pendidikan, alamat, rt, rw, kelurahan, kecamatan, kota, propinsi, kode_pos, id_stt, no_hp, no_rumah, id_asurat, id_tialamat, id_pekerjaan, kode_profesi, status_pekerjaan, nama_instansi, alamat_instansi, rt_dp, rw_dp, kelurahan_dp, kecamatan_dp, kota_dp, propinsi_dp, kodepos_dp, notelp_dp, suami_istri, nama_pydd, id_hubungan, alamat_pydd, kota_pydd, propinsi_pydd, notelp_pydd, sumber_dana, tp_dana, pp_tahun, ptp_tahun, max_spbulan, max_tpbulan, kjp, nisn', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Jk'=>array(self::BELONGS_TO,'JenisKelamin','id_jk'),
			'Kebangsaan'=>array(self::BELONGS_TO,'Kebangsaan','id_kebangsaan'),
			'StatusKawin'=>array(self::BELONGS_TO,'StatusKawin','id_sk'),
			'Agama'=>array(self::BELONGS_TO,'Agama','id_agama'),
			'Pendidikan'=>array(self::BELONGS_TO,'Pendidikan','id_pendidikan'),
			'Stt'=>array(self::BELONGS_TO,'StatusTtinggal','id_stt'),
			'AlamatSurat'=>array(self::BELONGS_TO,'AlamatSurat','id_asurat'),
			'TipeAlamat'=>array(self::BELONGS_TO,'TipeAlamat','id_tialamat'),
			'Pekerjaan'=>array(self::BELONGS_TO,'Pekerjaan','id_pekerjaan'),
			'KodeProfesi'=>array(self::BELONGS_TO,'KodeProfesi','kode_profesi'),
			'StatusPekerjaan'=>array(self::BELONGS_TO,'StatusPekerjaan','status_pekerjaan'),
			'Hubungan'=>array(self::BELONGS_TO,'Hubungan','id_hubungan'),
			'SumberDana'=>array(self::BELONGS_TO,'SumberDana','sumber_dana'),
			'Tpd'=>array(self::BELONGS_TO,'TujuanPdana','tp_dana'),
			'Ppt'=>array(self::BELONGS_TO,'PpTahun','pp_tahun'),
			'Ptp'=>array(self::BELONGS_TO,'PtpTahun','ptp_tahun'),
			'Maxspb'=>array(self::BELONGS_TO,'MaxSpbulan','max_spbulan'),
			'Sekolah'=>array(self::BELONGS_TO,'Sekolah','id_sekolah'),
			'Maxtpb'=>array(self::BELONGS_TO,'MaxTpbulan','max_tpbulan')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'id_jk' => 'Jenis Kelamin',
			'id_kebangsaan' => 'Kebangsaan',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'no_identitas' => 'No Identitas',
			'mb_identitas' => 'Masa Berlaku Identitas',
			'npwp' => 'NPWP',
			'nama_ibu' => 'Nama Ibu',
			'id_sk' => 'Status Kawin',
			'id_agama' => 'Agama',
			'id_pendidikan' => 'Pendidikan Terakhir',
			'alamat' => 'Alamat',
			'rt' => 'RT',
			'rw' => 'RW',
			'kelurahan' => 'Kelurahan',
			'kecamatan' => 'Kecamatan',
			'kota' => 'Kota',
			'propinsi' => 'Propinsi',
			'kode_pos' => 'Kode Pos',
			'id_stt' => 'Status Tempat Tinggal',
			'no_hp' => 'No Telepon Handphone',
			'no_rumah' => 'No Telepon Rumah',
			'id_asurat' => 'Alamat Surat',
			'id_tialamat' => 'Tipe Alamat',
			'id_pekerjaan' => 'Pekerjaan',
			'kode_profesi' => 'Kode Profesi',
			'status_pekerjaan' => 'Status Pekerjaan',
			'nama_instansi' => 'Nama Instansi (Sekolah)',
			'alamat_instansi' => 'Alamat Instansi (Sekolah)',
			'rt_dp' => 'RT',
			'rw_dp' => 'RW',
			'kelurahan_dp' => 'Kelurahan',
			'kecamatan_dp' => 'Kecamatan',
			'kota_dp' => 'Kota',
			'propinsi_dp' => 'Propinsi',
			'kodepos_dp' => 'Kode Pos',
			'notelp_dp' => 'No Telepon',
			'suami_istri' => 'Suami/Istri',
			'nama_pydd' => 'Nama',
			'id_hubungan' => 'Hubungan',
			'alamat_pydd' => 'Alamat',
			'kota_pydd' => 'Kota',
			'propinsi_pydd' => 'Propinsi',
			'notelp_pydd' => 'Notelp',
			'sumber_dana' => 'Sumber Dana',
			'tp_dana' => 'Tujuan Penggunaan Dana',
			'pp_tahun' => 'Penghasilan Per Tahun Tahun',
			'ptp_tahun' => 'Penghasilan Tambahan Per Tahun',
			'max_spbulan' => 'Max Setoran Per Bulan',
			'max_tpbulan' => 'Max Tarikan Per Bulan',
			'kjp' => 'No Rek KJP',
			'nisn' => 'NISN',
			'id_sekolah' => 'Sekolah',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('id_jk',$this->id_jk);
		$criteria->compare('id_kebangsaan',$this->id_kebangsaan);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('no_identitas',$this->no_identitas,true);
		$criteria->compare('mb_identitas',$this->mb_identitas,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('nama_ibu',$this->nama_ibu,true);
		$criteria->compare('id_sk',$this->id_sk);
		$criteria->compare('id_agama',$this->id_agama);
		$criteria->compare('id_pendidikan',$this->id_pendidikan);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('rt',$this->rt,true);
		$criteria->compare('rw',$this->rw,true);
		$criteria->compare('kelurahan',$this->kelurahan,true);
		$criteria->compare('kecamatan',$this->kecamatan,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('propinsi',$this->propinsi,true);
		$criteria->compare('kode_pos',$this->kode_pos,true);
		$criteria->compare('id_stt',$this->id_stt);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('no_rumah',$this->no_rumah,true);
		$criteria->compare('id_asurat',$this->id_asurat);
		$criteria->compare('id_tialamat',$this->id_tialamat);
		$criteria->compare('id_pekerjaan',$this->id_pekerjaan);
		$criteria->compare('kode_profesi',$this->kode_profesi);
		$criteria->compare('status_pekerjaan',$this->status_pekerjaan);
		$criteria->compare('nama_instansi',$this->nama_instansi,true);
		$criteria->compare('alamat_instansi',$this->alamat_instansi,true);
		$criteria->compare('rt_dp',$this->rt_dp,true);
		$criteria->compare('rw_dp',$this->rw_dp,true);
		$criteria->compare('kelurahan_dp',$this->kelurahan_dp,true);
		$criteria->compare('kecamatan_dp',$this->kecamatan_dp,true);
		$criteria->compare('kota_dp',$this->kota_dp,true);
		$criteria->compare('propinsi_dp',$this->propinsi_dp,true);
		$criteria->compare('kodepos_dp',$this->kodepos_dp,true);
		$criteria->compare('notelp_dp',$this->notelp_dp,true);
		$criteria->compare('suami_istri',$this->suami_istri,true);
		$criteria->compare('nama_pydd',$this->nama_pydd,true);
		$criteria->compare('id_hubungan',$this->id_hubungan);
		$criteria->compare('alamat_pydd',$this->alamat_pydd,true);
		$criteria->compare('kota_pydd',$this->kota_pydd,true);
		$criteria->compare('propinsi_pydd',$this->propinsi_pydd,true);
		$criteria->compare('notelp_pydd',$this->notelp_pydd,true);
		$criteria->compare('sumber_dana',$this->sumber_dana);
		$criteria->compare('tp_dana',$this->tp_dana);
		$criteria->compare('pp_tahun',$this->pp_tahun);
		$criteria->compare('ptp_tahun',$this->ptp_tahun);
		$criteria->compare('max_spbulan',$this->max_spbulan);
		$criteria->compare('max_tpbulan',$this->max_tpbulan);
		$criteria->compare('kjp',$this->kjp,true);
		$criteria->compare('nisn',$this->nisn,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('nominal',$this->nominal,true);
		$criteria->compare('jumlah_bulan',$this->jumlah_bulan,true);
		$criteria->compare('jumlah_nominal',$this->jumlah_nominal,true);
		$criteria->compare('alamat_sekolah',$this->alamat_sekolah,true);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('kelas',$this->kelas,true);
		
		if(Yii::app()->user->getState('role_id')==2)
			$criteria->compare('id_sekolah',Yii::app()->user->getState('id_sekolah'));
		else
			$criteria->compare('id_sekolah',$this->id_sekolah);
			
		if(isset(Yii::app()->session['tahun']) AND !empty(Yii::app()->session['tahun']))
			$criteria->compare('tahun',Yii::app()->session['tahun']);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchLengkap()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('persyaratan',$this->persyaratan);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('id_jk',$this->id_jk);
		$criteria->compare('id_kebangsaan',$this->id_kebangsaan);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('no_identitas',$this->no_identitas,true);
		$criteria->compare('mb_identitas',$this->mb_identitas,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('nama_ibu',$this->nama_ibu,true);
		$criteria->compare('id_sk',$this->id_sk);
		$criteria->compare('id_agama',$this->id_agama);
		$criteria->compare('id_pendidikan',$this->id_pendidikan);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('rt',$this->rt,true);
		$criteria->compare('rw',$this->rw,true);
		$criteria->compare('kelurahan',$this->kelurahan,true);
		$criteria->compare('kecamatan',$this->kecamatan,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('propinsi',$this->propinsi,true);
		$criteria->compare('kode_pos',$this->kode_pos,true);
		$criteria->compare('id_stt',$this->id_stt);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('no_rumah',$this->no_rumah,true);
		$criteria->compare('id_asurat',$this->id_asurat);
		$criteria->compare('id_tialamat',$this->id_tialamat);
		$criteria->compare('id_pekerjaan',$this->id_pekerjaan);
		$criteria->compare('kode_profesi',$this->kode_profesi);
		$criteria->compare('status_pekerjaan',$this->status_pekerjaan);
		$criteria->compare('nama_instansi',$this->nama_instansi,true);
		$criteria->compare('alamat_instansi',$this->alamat_instansi,true);
		$criteria->compare('rt_dp',$this->rt_dp,true);
		$criteria->compare('rw_dp',$this->rw_dp,true);
		$criteria->compare('kelurahan_dp',$this->kelurahan_dp,true);
		$criteria->compare('kecamatan_dp',$this->kecamatan_dp,true);
		$criteria->compare('kota_dp',$this->kota_dp,true);
		$criteria->compare('propinsi_dp',$this->propinsi_dp,true);
		$criteria->compare('kodepos_dp',$this->kodepos_dp,true);
		$criteria->compare('notelp_dp',$this->notelp_dp,true);
		$criteria->compare('suami_istri',$this->suami_istri,true);
		$criteria->compare('nama_pydd',$this->nama_pydd,true);
		$criteria->compare('id_hubungan',$this->id_hubungan);
		$criteria->compare('alamat_pydd',$this->alamat_pydd,true);
		$criteria->compare('kota_pydd',$this->kota_pydd,true);
		$criteria->compare('propinsi_pydd',$this->propinsi_pydd,true);
		$criteria->compare('notelp_pydd',$this->notelp_pydd,true);
		$criteria->compare('sumber_dana',$this->sumber_dana);
		$criteria->compare('tp_dana',$this->tp_dana);
		$criteria->compare('pp_tahun',$this->pp_tahun);
		$criteria->compare('ptp_tahun',$this->ptp_tahun);
		$criteria->compare('max_spbulan',$this->max_spbulan);
		$criteria->compare('max_tpbulan',$this->max_tpbulan);
		$criteria->compare('kjp',$this->kjp,true);
		$criteria->compare('nisn',$this->nisn,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('nominal',$this->nominal,true);
		$criteria->compare('jumlah_bulan',$this->jumlah_bulan,true);
		$criteria->compare('jumlah_nominal',$this->jumlah_nominal,true);
		$criteria->compare('alamat_sekolah',$this->alamat_sekolah,true);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('kelas',$this->kelas,true);
		
		if(Yii::app()->user->getState('role_id')==2)
			$criteria->compare('id_sekolah',Yii::app()->user->getState('id_sekolah'));
		else
			$criteria->compare('id_sekolah',$this->id_sekolah);
		
		if(isset(Yii::app()->session['tahun']) AND !empty(Yii::app()->session['tahun']))
			$criteria->compare('tahun',Yii::app()->session['tahun']);
		
		$criteria->addCondition("persyaratan = 1");
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchSyaratBelumLengkap()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('persyaratan',$this->persyaratan);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('id_jk',$this->id_jk);
		$criteria->compare('id_kebangsaan',$this->id_kebangsaan);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('no_identitas',$this->no_identitas,true);
		$criteria->compare('mb_identitas',$this->mb_identitas,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('nama_ibu',$this->nama_ibu,true);
		$criteria->compare('id_sk',$this->id_sk);
		$criteria->compare('id_agama',$this->id_agama);
		$criteria->compare('id_pendidikan',$this->id_pendidikan);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('rt',$this->rt,true);
		$criteria->compare('rw',$this->rw,true);
		$criteria->compare('kelurahan',$this->kelurahan,true);
		$criteria->compare('kecamatan',$this->kecamatan,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('propinsi',$this->propinsi,true);
		$criteria->compare('kode_pos',$this->kode_pos,true);
		$criteria->compare('id_stt',$this->id_stt);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('no_rumah',$this->no_rumah,true);
		$criteria->compare('id_asurat',$this->id_asurat);
		$criteria->compare('id_tialamat',$this->id_tialamat);
		$criteria->compare('id_pekerjaan',$this->id_pekerjaan);
		$criteria->compare('kode_profesi',$this->kode_profesi);
		$criteria->compare('status_pekerjaan',$this->status_pekerjaan);
		$criteria->compare('nama_instansi',$this->nama_instansi,true);
		$criteria->compare('alamat_instansi',$this->alamat_instansi,true);
		$criteria->compare('rt_dp',$this->rt_dp,true);
		$criteria->compare('rw_dp',$this->rw_dp,true);
		$criteria->compare('kelurahan_dp',$this->kelurahan_dp,true);
		$criteria->compare('kecamatan_dp',$this->kecamatan_dp,true);
		$criteria->compare('kota_dp',$this->kota_dp,true);
		$criteria->compare('propinsi_dp',$this->propinsi_dp,true);
		$criteria->compare('kodepos_dp',$this->kodepos_dp,true);
		$criteria->compare('notelp_dp',$this->notelp_dp,true);
		$criteria->compare('suami_istri',$this->suami_istri,true);
		$criteria->compare('nama_pydd',$this->nama_pydd,true);
		$criteria->compare('id_hubungan',$this->id_hubungan);
		$criteria->compare('alamat_pydd',$this->alamat_pydd,true);
		$criteria->compare('kota_pydd',$this->kota_pydd,true);
		$criteria->compare('propinsi_pydd',$this->propinsi_pydd,true);
		$criteria->compare('notelp_pydd',$this->notelp_pydd,true);
		$criteria->compare('sumber_dana',$this->sumber_dana);
		$criteria->compare('tp_dana',$this->tp_dana);
		$criteria->compare('pp_tahun',$this->pp_tahun);
		$criteria->compare('ptp_tahun',$this->ptp_tahun);
		$criteria->compare('max_spbulan',$this->max_spbulan);
		$criteria->compare('max_tpbulan',$this->max_tpbulan);
		$criteria->compare('kjp',$this->kjp,true);
		$criteria->compare('nisn',$this->nisn,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('nominal',$this->nominal,true);
		$criteria->compare('jumlah_bulan',$this->jumlah_bulan,true);
		$criteria->compare('jumlah_nominal',$this->jumlah_nominal,true);
		$criteria->compare('alamat_sekolah',$this->alamat_sekolah,true);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('kelas',$this->kelas,true);
		
		if(Yii::app()->user->getState('role_id')==2)
			$criteria->compare('id_sekolah',Yii::app()->user->getState('id_sekolah'));
		else
			$criteria->compare('id_sekolah',$this->id_sekolah);
		
		if(isset(Yii::app()->session['tahun']) AND !empty(Yii::app()->session['tahun']))
			$criteria->compare('tahun',Yii::app()->session['tahun']);
		
		$criteria->addCondition("persyaratan = 0");
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchCair()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('persyaratan',$this->persyaratan);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('id_jk',$this->id_jk);
		$criteria->compare('id_kebangsaan',$this->id_kebangsaan);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('no_identitas',$this->no_identitas,true);
		$criteria->compare('mb_identitas',$this->mb_identitas,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('nama_ibu',$this->nama_ibu,true);
		$criteria->compare('id_sk',$this->id_sk);
		$criteria->compare('id_agama',$this->id_agama);
		$criteria->compare('id_pendidikan',$this->id_pendidikan);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('rt',$this->rt,true);
		$criteria->compare('rw',$this->rw,true);
		$criteria->compare('kelurahan',$this->kelurahan,true);
		$criteria->compare('kecamatan',$this->kecamatan,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('propinsi',$this->propinsi,true);
		$criteria->compare('kode_pos',$this->kode_pos,true);
		$criteria->compare('id_stt',$this->id_stt);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('no_rumah',$this->no_rumah,true);
		$criteria->compare('id_asurat',$this->id_asurat);
		$criteria->compare('id_tialamat',$this->id_tialamat);
		$criteria->compare('id_pekerjaan',$this->id_pekerjaan);
		$criteria->compare('kode_profesi',$this->kode_profesi);
		$criteria->compare('status_pekerjaan',$this->status_pekerjaan);
		$criteria->compare('nama_instansi',$this->nama_instansi,true);
		$criteria->compare('alamat_instansi',$this->alamat_instansi,true);
		$criteria->compare('rt_dp',$this->rt_dp,true);
		$criteria->compare('rw_dp',$this->rw_dp,true);
		$criteria->compare('kelurahan_dp',$this->kelurahan_dp,true);
		$criteria->compare('kecamatan_dp',$this->kecamatan_dp,true);
		$criteria->compare('kota_dp',$this->kota_dp,true);
		$criteria->compare('propinsi_dp',$this->propinsi_dp,true);
		$criteria->compare('kodepos_dp',$this->kodepos_dp,true);
		$criteria->compare('notelp_dp',$this->notelp_dp,true);
		$criteria->compare('suami_istri',$this->suami_istri,true);
		$criteria->compare('nama_pydd',$this->nama_pydd,true);
		$criteria->compare('id_hubungan',$this->id_hubungan);
		$criteria->compare('alamat_pydd',$this->alamat_pydd,true);
		$criteria->compare('kota_pydd',$this->kota_pydd,true);
		$criteria->compare('propinsi_pydd',$this->propinsi_pydd,true);
		$criteria->compare('notelp_pydd',$this->notelp_pydd,true);
		$criteria->compare('sumber_dana',$this->sumber_dana);
		$criteria->compare('tp_dana',$this->tp_dana);
		$criteria->compare('pp_tahun',$this->pp_tahun);
		$criteria->compare('ptp_tahun',$this->ptp_tahun);
		$criteria->compare('max_spbulan',$this->max_spbulan);
		$criteria->compare('max_tpbulan',$this->max_tpbulan);
		$criteria->compare('kjp',$this->kjp,true);
		$criteria->compare('nisn',$this->nisn,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('nominal',$this->nominal,true);
		$criteria->compare('jumlah_bulan',$this->jumlah_bulan,true);
		$criteria->compare('jumlah_nominal',$this->jumlah_nominal,true);
		$criteria->compare('alamat_sekolah',$this->alamat_sekolah,true);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('kelas',$this->kelas,true);
		
		if(Yii::app()->user->getState('role_id')==2)
			$criteria->compare('id_sekolah',Yii::app()->user->getState('id_sekolah'));
		else
			$criteria->compare('id_sekolah',$this->id_sekolah);
		
		if(isset(Yii::app()->session['tahun']) AND !empty(Yii::app()->session['tahun']))
			$criteria->compare('tahun',Yii::app()->session['tahun']);
			
		$criteria->addCondition("cair = 1");
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DataNasabah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getRelation($relation,$value)
	{
		if(!empty($this->$relation))
			return $this->$relation->$value;
		else
			return NULL;
	}
	
	public function statusSyarat($i)
	{
		if($i==0)
			return "Belum diproses";
		else if($i==1)
			return "Sedang diproses";
		else 
			return "Sudah diproses";
	}
	
	public function persyaratan($i)
	{
		if($i==0)
			return "Belum Lengkap";
		if($i==1)
			return "Lengkap";
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}
	
	public function getTahun()
	{
		$list = array();
		
		foreach(DataNasabah::model()->findAll() as $data)
		{
			$list[$data->tahun]=$data->tahun;
			$list = $list;
		}
		
		return $list;
	}
}
