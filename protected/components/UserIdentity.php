<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user=User::model()->findByAttributes(array('username'=>$this->username));
		
		if($user!==null) 
		{
			if($user->password===$this->password)
			{
				date_default_timezone_set('Asia/Jakarta');
				$user->last_login = date('Y-m-d H:i:s');
				$user->save();
				$this->setState('user_id',$user->id);
				$this->setState('role_id',$user->role_id);
				$this->setState('id_sekolah',$user->id_sekolah);
			
				$this->errorCode=self::ERROR_NONE;
				
			} else {
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}	
				
		} else {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		} 
							
		return !$this->errorCode;
		
	}
}