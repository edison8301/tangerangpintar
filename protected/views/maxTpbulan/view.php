<?php
$this->breadcrumbs=array(
	'Max Tarikan Per Bulan'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Tarikan Per Bulan','url'=>array('index')),
array('label'=>'Tambah Tarikan Per Bulan','url'=>array('create')),
array('label'=>'Perbaharui Tarikan Per Bulan','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Tarikan Per Bulan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Tarikan Per Bulan','url'=>array('admin')),
);
?>

<h1>Lihat Tarikan Per Bulan #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'max_tpbulan',
),
)); ?>
