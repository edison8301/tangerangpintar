<?php
$this->breadcrumbs=array(
	'Max Tarikan Per Bulan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'List Data Tarikan Per Bulan','url'=>array('index')),
	array('label'=>'Create Data Tarikan Per Bulan','url'=>array('create')),
	array('label'=>'View Data Tarikan Per Bulan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Data Tarikan Per Bulan','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Max Tarikan Per Bulan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>