<?php
$this->breadcrumbs=array(
	'Role Access'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Role Access','url'=>array('index')),
	array('label'=>'Tambah Role Access','url'=>array('create')),
	array('label'=>'Lihat Role Access','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Role Access','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Role Access <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>