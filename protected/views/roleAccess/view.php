<?php
$this->breadcrumbs=array(
	'Role Accesse'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar RoleAccess','url'=>array('index')),
array('label'=>'Tambah RoleAccess','url'=>array('create')),
array('label'=>'Perbaharui RoleAccess','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus RoleAccess','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola RoleAccess','url'=>array('admin')),
);
?>

<h1>Lihat Role Access #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'role_id',
		'access_id',
		'status',
),
)); ?>
