<div class="margin20" id="page-read">

<h2><?php print $model->title; ?></h2>

<hr>

<?php print $model->content; ?>

<div class='clearfix'>&nbsp;</div>

<table class='table'>
<tr>
	<th>No</th>
	<th>Judul File</th>
	<th>Unduh</th>
</tr>
<?php $i=1; foreach($model->getDataDownload() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->title; ?></td>
	<td>
		<?php print CHtml::link('<i class="glyphicon glyphicon-download-alt"></i>',Yii::app()->request->baseUrl.'/uploads/download/'.$data->file,array('target'=>'_blank')); ?>
	</td>
</tr>
<?php $i++; } ?>
</table>

</div>

