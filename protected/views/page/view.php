<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	$model->title,
);

?>

<h1>Lihat Laman</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'context'=>'primary',
		'icon'=>'pencil white',
		'url'=>array('/page/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Baca',
		'context'=>'primary',
		'icon'=>'search white',
		'url'=>array('/page/read','id'=>$model->id),
		'htmlOptions'=>array('target'=>'_blank')
)); ?>&nbsp;
<?php $this->widget('bootstrap.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola',
		'context'=>'primary',
		'icon'=>'list white',
		'url'=>array('/page/admin')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'type'=>'striped bordered',
	'attributes'=>array(
		'title',
		array(
			'label'=>'Content',
			'type'=>'raw',
			'value'=>$model->content
		),
		/*
		array(
			'label'=>'Url',
			'type'=>'raw',
			'value'=>$this->createUrl('page/read',array('id'=>$model->id))
		),
		*/
		'time_created',
	),
)); ?>

<h2>File Unduhan</h2>
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/download/create','model'=>'Page','model_id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<table class='table'>
<tr>
	<th style="width:8%">No</th>
	<th>Judul</th>
	<th>Unduh</th>
	<th>Action</th>
</tr>
<?php $i=1; foreach($model->getDataDownload() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->title; ?></td>
	<td>
		<?php print CHtml::link('<i class="glyphicon glyphicon-download-alt"></i>',Yii::app()->baseUrl.'/uploads/download/'.$data->file,array('target'=>'_blank')); ?>
	</td>
	<td>
		<?php print CHtml::link('<i class="glyphicon glyphicon-pencil"></i>',array('download/update','id'=>$data->id)); ?>
		<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>',array('download/hapus','id'=>$data->id)); ?>
	</td>
</tr>
<?php $i++; } ?>
</table>