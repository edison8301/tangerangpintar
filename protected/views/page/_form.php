<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'page-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->labelEx($model,'content'); ?>
	<?php $this->widget('ext.editMe.widgets.ExtEditMe', array(
			'model'=>$model,
			'attribute'=>'content',
			'filebrowserImageBrowseUrl'=>  Yii::app()->baseUrl.'/vendors/kcfinder/browse.php',
			'filebrowserImageBrowseLinkUrl'=>Yii::app()->baseUrl.'/vendors/kcfinder/browse.php?type=images',
			'filebrowserFlashBrowseUrl' => Yii::app()->baseUrl.'/vendors/kcfinder/browse.php?type=Flash',
	)); ?>
	<?php echo $form->error($model,'content'); ?> 

	<div>&nbsp;</div>
	
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
		)); ?>
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'url'=>array('page/admin'),
			'context'=>'primary',
			'icon'=>'list white',
			'label'=>'Kelola',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
