<?php
	$video = PageVideo::model()->findByAttributes(array('page_id'=>$model->id));
	
	if($video===null)
	{
		$video = new PageVideo;
		$video->page_id = $model->id;
		$video->save();
	}

?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'buttons'=>array(
		array('label'=>'Pilihan Menu','items'=>array(
			array('label'=>'Sunting Video','icon'=>'pencil','url'=>array('/pageVideo/update','id'=>$video->id)),
		)),
    ),
)); ?>


<div>&nbsp;</div>

<table class='table table-bordered table-striped'>
<tr>
	<th>MP4</th>
	<td>
		<?php if($video->mp4!='') { ?>
		<?php $this->widget('application.extensions.videojs.EVideoJS', array(
				'options' => array(
					'id' => false,
					'width' => 360,
					'height' => 270,
					'poster' => "http://localhost/pu-babel/images/playvido.png",
					'video_mp4' => Yii::app()->request->baseUrl."/uploads/pekerjaan/".$video->mp4,
					'video_ogv' => false,
					'video_webm' => false,
					'flash_fallback' => true,
					'flash_player' => 'http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf',
					'controls' => true,
					'preload' => true,
					'autoplay' => false,
					'support' => true,
					'download' => false,
				),
		)); ?>
		<?php } ?>
	</td>
</tr>
<tr>
	<th>OGV</th>
	<td>
		<?php if($video->ogv!='') { ?>
		<?php $this->widget('application.extensions.videojs.EVideoJS', array(
				'options' => array(
					'id' => false,
					'width' => 360,
					'height' => 270,
					'poster' => "http://localhost/pu-babel/images/playvido.png",
					'video_mp4' => false,
					'video_ogv' => Yii::app()->request->baseUrl."/uploads/pekerjaan/".$video->ogv,
					'video_webm' => false,
					'flash_fallback' => true,
					'flash_player' => 'http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf',
					'controls' => true,
					'preload' => true,
					'autoplay' => false,
					'support' => true,
					'download' => false,
				),
		)); ?>
		<?php } ?>
	</td>
</tr>
<tr>
	<th>WebM</th>
	<td>
		<?php if($video->webm!='') { ?>
		<?php $this->widget('application.extensions.videojs.EVideoJS', array(
				'options' => array(
					'id' => false,
					'width' => 360,
					'height' => 270,
					'poster' => "http://localhost/pu-babel/images/playvido.png",
					'video_mp4' => false,
					'video_ogv' => false,
					'video_webm' => Yii::app()->request->baseUrl."/uploads/pekerjaan/".$video->webm,
					'flash_fallback' => true,
					'flash_player' => 'http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf',
					'controls' => true,
					'preload' => true,
					'autoplay' => false,
					'support' => true,
					'download' => false,
				),
		)); ?>
		<?php } ?>
	</td>
</tr>
</table>