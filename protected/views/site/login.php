<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<?php $this->beginWidget('booster.widgets.TbPanel',array(
        'title' => 'Login Kartu Pintar Kabupaten Tangerang',
        'headerIcon' => 'lock',
		'context'=>'primary',
    	'padContent' => false,
        'htmlOptions' => array('class' => 'bootstrap-widget-table','style'=>'margin-top:120px')
));?>

<div id="site-login">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
)); ?>
			
		<?php echo $form->textFieldGroup($model,'username'); ?>

		<?php echo $form->passwordFieldGroup($model,'password'); ?>
		
		<?php echo $form->dropDownListGroup($model,'tahun',array('widgetOptions'=>array('data'=>array('2015'=>'2015','2014'=>'2014'),'htmlOptions'=>array('empty'=>'-- Pilih Tahun --')))); ?>
		
		<div class="rememberMe">
			<?php echo $form->checkBox($model,'rememberMe'); ?>
			<?php echo $form->label($model,'rememberMe'); ?>
			<?php echo $form->error($model,'rememberMe'); ?>
		</div>

		<div class="buttons">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'label'=>'Login',
					'icon'=>'lock white',
			)); ?>
		</div>

<?php $this->endWidget(); ?>

</div>

<?php $this->endWidget(); ?>
