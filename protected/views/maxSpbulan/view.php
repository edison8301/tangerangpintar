<?php
$this->breadcrumbs=array(
	'Max Setoran Per Bulan'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Max Setoran Per Bulan','url'=>array('index')),
array('label'=>'Tambah Max Setoran Per Bulan','url'=>array('create')),
array('label'=>'Perbaharui Max Setoran Per Bulan','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Max Setoran Per Bulan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Max Setoran Per Bulan','url'=>array('admin')),
);
?>

<h1>Lihat Max Setoran Per Bulan #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'max_spbulan',
),
)); ?>
