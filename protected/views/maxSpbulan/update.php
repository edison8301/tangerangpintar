<?php
$this->breadcrumbs=array(
	'Max Setoran Per Bulan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Max Setoran Per Bulan','url'=>array('index')),
	array('label'=>'Tambah Data Max Setoran Per Bulan','url'=>array('create')),
	array('label'=>'Lihat Data Max Setoran Per Bulan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Max Setoran Per Bulan','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Max Setoran Per Bulan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>