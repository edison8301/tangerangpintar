<?php
$this->breadcrumbs=array(
	'User'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

$this->menu=array(
	array('label'=>'Daftar User','url'=>array('index')),
	array('label'=>'Tambah User','url'=>array('create')),
	array('label'=>'Lihat User','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola User','url'=>array('admin')),
);

?>

	<h1>Perbaharui User </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>