<?php
$this->breadcrumbs=array(
	'User'=>array('index'),
	$model->username,
);
/*
$this->menu=array(
	array('label'=>'Daftar User','url'=>array('index')),
	array('label'=>'Tambah User','url'=>array('create')),
	array('label'=>'Perbaharui User','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus User','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola User','url'=>array('admin')),
);
*/
?>

<h1>Lihat User</h1>

<?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'link',
		'context'=>'primary',
		'url'=>array('user/update','id'=>$model->id),
		'label' => 'Sunting User',
		'icon'=>'pencil',
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'link',
		'context'=>'primary',
		'url'=>array('user/admin'),
		'label' => 'Data User',
		'icon'=>'list',
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'username',
			'password',
			array(
				'label'=>'Role',
				'value'=>$model->Role->nama
			),
			array(
				'label'=>'Sekolah',
				'value'=>$model->getRelation("Sekolah","nama_sekolah"),
				'visible'=>$model->role_id == 1 ? "0" : "1"
			),
			array(
				'label'=>'Status',
				'value'=>$model->status == 1 ? "Aktif" : "Nonaktif"
			),
			'last_login',
		),
)); ?>
