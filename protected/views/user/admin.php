<?php
$this->breadcrumbs=array(
	'User'=>array('index'),
	'Kelola',
);

//$this->menu=array(
//array('label'=>'List Data Nasabah','url'=>array('index')),
//array('label'=>'Create Data Nasabah','url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('data-user-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>DATA USER</h1>

<?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'link',
		'context'=>'primary',
		'url'=>array('user/create'),
		'label' => 'Tambah',
		'icon'=>'plus',
)); ?>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'data-user-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'username',
			'password',
			array(
				'class'=>'CDataColumn',
				'name'=>'role_id',
				'header'=>'Role',
				'value'=>'$data->Role->nama'
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_sekolah',
				'header'=>'Sekolah',
				'value'=>'$data->getRelation("Sekolah","nama_sekolah")'
			),
			'last_login',
			array(
				'class'=>'CDataColumn',
				'name'=>'status',
				'header'=>'Status',
				'type'=>'raw',
				'value'=>'$data->status == 1 ? "Aktif" : "Nonaktif"',
				'filter'=>array('1'=>'Aktif','0'=>'Nonaktif')
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>