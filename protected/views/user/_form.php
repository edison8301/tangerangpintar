<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'username',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->passwordFieldGroup($model,'password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->dropDownListGroup($model,'role_id',array('widgetOptions'=>array('data' => CHtml::listData(Role::model()->findAll(),'id','nama')))); ?>
	
	<?php echo $form->dropDownListGroup($model,'id_sekolah',array('widgetOptions'=>array('data' => CHtml::listData(Sekolah::model()->findAll(),'id','nama_sekolah')))); ?>

	<?php echo $form->dropDownListGroup($model,'status',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5'),'data'=>array('0'=>'Nonaktif','1'=>'Aktif')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
