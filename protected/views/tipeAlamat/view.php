<?php
$this->breadcrumbs=array(
	'Tipe Alamat'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Tipe Alamat','url'=>array('index')),
array('label'=>'Tambah Tipe Alamat','url'=>array('create')),
array('label'=>'Perbaharui Tipe Alamat','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Tipe Alamat','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Tipe Alamat','url'=>array('admin')),
);
?>

<h1>Lihat Tipe Alamat #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'tipe_alamat',
),
)); ?>
