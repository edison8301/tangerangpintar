<?php
$this->breadcrumbs=array(
	'Tipe Alamat'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Tipe Alamat','url'=>array('index')),
	array('label'=>'Tambah Data Tipe Alamat','url'=>array('create')),
	array('label'=>'Lihat Data Tipe Alamat','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Tipe Alamat','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Tipe Alamat <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>