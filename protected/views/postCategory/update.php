<?php
$this->breadcrumbs=array(
	'Post Categories'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	//array('label'=>'List PostCategory','url'=>array('index')),
	array('label'=>'Tambah Kategori','url'=>array('create')),
	array('label'=>'Lihat Kategori','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Kategori','url'=>array('admin')),
	);
	?>

<h1>Sunting Kategori Artikel</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>