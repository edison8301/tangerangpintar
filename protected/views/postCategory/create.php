<?php
$this->breadcrumbs=array(
	'Post Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Kelola Kategori','url'=>array('admin')),
	array('label'=>'Kelola Post','url'=>array('admin'),'icon'=>'list'),
);
?>

<h1>Tambah Kategori Post</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>