<?php
$this->breadcrumbs=array(
	'Status Pekerjaan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Status Pekerjaan','url'=>array('index')),
	array('label'=>'Tambah Status Pekerjaan','url'=>array('create')),
	array('label'=>'Lihat Status Pekerjaan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Status Pekerjaan','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Status Pekerjaan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>