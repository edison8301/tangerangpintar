<?php
$this->breadcrumbs=array(
	'Status Pekerjaan'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar StatusPekerjaan','url'=>array('index')),
array('label'=>'Tambah StatusPekerjaan','url'=>array('create')),
array('label'=>'Perbaharui StatusPekerjaan','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus StatusPekerjaan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola StatusPekerjaan','url'=>array('admin')),
);
?>

<h1>Lihat Status Pekerjaan #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'status_pekerjaan',
),
)); ?>
