<?php
$this->breadcrumbs=array(
	'Status Pekerjaan'=>array('index'),
	'Tambah',
);

/*$this->menu=array(
array('label'=>'List Kebangsaan','url'=>array('index')),
array('label'=>'Kelola Kebangsaan','url'=>array('admin')),
); */
?>

<h1>Tambah Status Pekerjaan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>