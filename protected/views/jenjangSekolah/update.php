<?php
$this->breadcrumbs=array(
	'Jenjang Sekolahs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List JenjangSekolah','url'=>array('index')),
	array('label'=>'Create JenjangSekolah','url'=>array('create')),
	array('label'=>'View JenjangSekolah','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage JenjangSekolah','url'=>array('admin')),
	);
	?>

	<h1>Update JenjangSekolah <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>