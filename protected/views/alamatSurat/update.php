<?php
$this->breadcrumbs=array(
	'Alamat Surat'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Alamat Surat','url'=>array('index')),
	array('label'=>'Tambah Data Alamat Surat','url'=>array('create')),
	array('label'=>'Lihat Data Alamat Surat','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Alamat Surat','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Alamat Surat <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>