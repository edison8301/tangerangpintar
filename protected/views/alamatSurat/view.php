<?php
$this->breadcrumbs=array(
	'Alamat Surat'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Data Alamat Surat','url'=>array('index')),
array('label'=>'Tambah Data Alamat Surat','url'=>array('create')),
array('label'=>'Perbaharui Data Alamat Surat','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Data Alamat Surat','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Data Alamat Surat','url'=>array('admin')),
);
?>

<h1>Lihat Data Agama #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'alamat_surat'
),
)); ?>
