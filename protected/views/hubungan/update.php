<?php
$this->breadcrumbs=array(
	'Hubungan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Hubungan','url'=>array('index')),
	array('label'=>'Tambah Data Hubungan','url'=>array('create')),
	array('label'=>'Lihat Data Hubungan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Hubungan','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Hubungan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>