<?php
$this->breadcrumbs=array(
	'Hubungans'=>array('index'),
	'Tambah',
);

$this->menu=array(
array('label'=>'Daftar Hubungan','url'=>array('index')),
array('label'=>'Kelola Hubungan','url'=>array('admin')),
);
?>

<h1>Tambah Hubungan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>