<?php
$this->breadcrumbs=array(
	'Hubungans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Hubungan','url'=>array('index')),
array('label'=>'Tambah Hubungan','url'=>array('create')),
array('label'=>'Perbaharui Hubungan','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Hubungan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Hubungan','url'=>array('admin')),
);
?>

<h1>View Hubungan #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'hubungan',
),
)); ?>
