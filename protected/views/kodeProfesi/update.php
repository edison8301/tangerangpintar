<?php
$this->breadcrumbs=array(
	'Kode Profesi'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Kode Profesi','url'=>array('index')),
	array('label'=>'Tambah Data Kode Profesi','url'=>array('create')),
	array('label'=>'Lihat Data Kode Profesi','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Kode Profesi','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Kode Profesi <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>