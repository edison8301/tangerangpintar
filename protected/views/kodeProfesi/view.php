<?php
$this->breadcrumbs=array(
	'Kode Profesi'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar KodeProfesi','url'=>array('index')),
array('label'=>'Tambah KodeProfesi','url'=>array('create')),
array('label'=>'Perbaharui KodeProfesi','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus KodeProfesi','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola KodeProfesi','url'=>array('admin')),
);
?>

<h1>Lihat Kode Profesi #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'kode_profesi',
),
)); ?>
