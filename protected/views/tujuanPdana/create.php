<?php
$this->breadcrumbs=array(
	'Tujuan Penggunaan Dana'=>array('index'),
	'Tambah',
);

/*$this->menu=array(
array('label'=>'List Kebangsaan','url'=>array('index')),
array('label'=>'Kelola Kebangsaan','url'=>array('admin')),
); */
?>

<h1>Tambah Tujuan Penggunaan Dana</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>