<?php
$this->breadcrumbs=array(
	'Tujuan Penggunaan Dana'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Tujuan Penggunaan Dana','url'=>array('index')),
array('label'=>'Tambah Tujuan Penggunaan Dana','url'=>array('create')),
array('label'=>'Perbaharui Tujuan Penggunaan Dana','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Tujuan Penggunaan Dana','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Tujuan Penggunaan Dana','url'=>array('admin')),
);
?>

<h1>Lihat Tujuan Penggunaan Dana #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'tujuan_pdana',
),
)); ?>
