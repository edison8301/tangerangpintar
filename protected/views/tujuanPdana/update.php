<?php
$this->breadcrumbs=array(
	'Tujuan Penggunaan Dana'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Tujuan Penggunaan Dana','url'=>array('index')),
	array('label'=>'Create Data Tujuan Penggunaan Dana','url'=>array('create')),
	array('label'=>'View Data Tujuan Penggunaan Dana','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Data Tujuan Penggunaan Dana','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Tujuan Penggunaan Dana <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>