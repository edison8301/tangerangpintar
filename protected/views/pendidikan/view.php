<?php
$this->breadcrumbs=array(
	'Pendidikan'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Pendidikan','url'=>array('index')),
array('label'=>'Tambah Pendidikan','url'=>array('create')),
array('label'=>'Perbaharui Pendidikan','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Pendidikan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Pendidikan','url'=>array('admin')),
);
?>

<h1>Lihat Pendidikan #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'pendidikan',
),
)); ?>
