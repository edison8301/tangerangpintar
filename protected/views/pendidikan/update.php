<?php
$this->breadcrumbs=array(
	'Pendidikan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Pendidikan','url'=>array('index')),
	array('label'=>'Tambah Data Pendidikan','url'=>array('create')),
	array('label'=>'Lihat Data Pendidikan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Pendidikan','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Pendidikan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>