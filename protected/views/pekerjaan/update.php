<?php
$this->breadcrumbs=array(
	'Pekerjaan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Pekerjaan','url'=>array('index')),
	array('label'=>'Tambah Data Pekerjaan','url'=>array('create')),
	array('label'=>'Lihat Data Pekerjaan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Pekerjaan','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Pekerjaan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>