<?php
$this->breadcrumbs=array(
	'Pekerjaan'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Pekerjaan','url'=>array('index')),
array('label'=>'Tambah Pekerjaan','url'=>array('create')),
array('label'=>'Perbaharui Pekerjaan','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Pekerjaan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Pekerjaan','url'=>array('admin')),
);
?>

<h1>Lihat Data Pekerjaan #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'pekerjaan',
),
)); ?>
