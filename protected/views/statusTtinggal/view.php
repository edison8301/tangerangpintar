<?php
$this->breadcrumbs=array(
	'Status Tempat Tinggal'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Status Tempat Tinggal','url'=>array('index')),
array('label'=>'Tambah StatusT Tinggal','url'=>array('create')),
array('label'=>'Perbaharui Status Tempat Tinggal','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Status Tempat Tinggal','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Status Tempat Tinggal','url'=>array('admin')),
);
?>

<h1>Lihat Status Tempat Tinggal #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'status_ttinggal',
),
)); ?>
