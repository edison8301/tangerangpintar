<?php
$this->breadcrumbs=array(
	'Status Tempat Tinggal'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Status Tempat tinggal','url'=>array('index')),
	array('label'=>'Tambah Data Status Tempat tinggal','url'=>array('create')),
	array('label'=>'Lihat Data Status Tempat tinggal','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Status Tempat tinggal','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Status Tempat Tinggal <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>