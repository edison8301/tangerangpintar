<?php
$this->breadcrumbs=array(
	'Slides'=>array('index'),
	$model->title,
);

?>

<h1>Lihat Slide</h1>

<?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'pencil white','label'=>'Sunting','url'=>array('slide/update','id'=>$model->id))); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'plus white','label'=>'Tambah','url'=>array('slide/create'))); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'list white','label'=>'Kelola','url'=>array('slide/admin'))); ?>






<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'type'=>'striped bordered',
	'attributes'=>array(
		'title',
		array(
			'label'=>'File',
			'context'=>'raw',
			'value'=>CHtml::image(Yii::app()->request->baseUrl.'/uploads/slide/'.$model->file)
		),
		'time_created',
	),
)); ?>
