<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div id="mainmenu">
<?php if(Yii::app()->user->getState('role_id')==1) { ?>
<?php $this->widget('booster.widgets.TbNavbar',array(
        'brand' => 'Tangerang Pintar',
        'fixed' => false,
    	'fluid' => true,
		'type'=>'inverse',
        'items' => array(
            array(
                'class' => 'booster.widgets.TbMenu',
            	'type' => 'navbar',
                'items' => array(
                    array('label' => 'Dashboard', 'icon'=>'home', 'url' => array('dataNasabah/index'),'visible'=>!Yii::app()->user->isGuest),
                    array('label' => 'Data Usulan KPKT', 'icon'=>'list', 'url' => array('dataNasabah/admin'),'visible'=>!Yii::app()->user->isGuest),
					array('label' => 'Pencairan Data KPKT', 'icon'=>'ok', 'url' => array('dataNasabah/cair'),'visible'=>!Yii::app()->user->isGuest),
					array('label' => 'Data Sekolah', 'icon'=>'tags', 'url' => array('sekolah/admin')),
					array('label' => 'Data User', 'icon'=>'user', 'url' => array('user/admin')),
					array('label' => 'Laman', 'icon'=>'file', 'url' => array('page/admin')),
					array('label' => 'Berita', 'icon'=>'book', 'url' => array('post/admin')),
					array('label' => 'Slide', 'icon'=>'play', 'url' => array('slide/admin')),
					array('label' => 'Logout','icon'=>'off', 'url' => array('site/logout'),'visible'=>!Yii::app()->user->isGuest),
                )
            )
        )
)); ?>
<?php } else { ?>
<?php $this->widget('booster.widgets.TbNavbar',array(
        'brand' => 'Tangerang Pintar',
        'fixed' => false,
		'type'=>'inverse',
    	'fluid' => true,
        'items' => array(
            array(
                'class' => 'booster.widgets.TbMenu',
            	'type' => 'navbar',
                'items' => array(
                    array('label' => 'Dashboard', 'icon'=>'home', 'url' => array('dataNasabah/index'),'visible'=>!Yii::app()->user->isGuest),
                    array('label' => 'Data Usulan KPKT', 'icon'=>'list', 'url' => array('dataNasabah/usulan'),'visible'=>!Yii::app()->user->isGuest),
					array('label' => 'Pencairan Data KPKT', 'icon'=>'ok', 'url' => array('dataNasabah/cair'),'visible'=>!Yii::app()->user->isGuest),
					array('label' => 'Profil Sekolah', 'icon'=>'user', 'url' => array('sekolah/profil')),
					array('label' => 'Logout', 'icon'=>'off', 'url' => array('site/logout'),'visible'=>!Yii::app()->user->isGuest),
                )
            )
        )
)); ?>

<?php } ?>
</div>

<div id="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php if(isset($this->breadcrumbs)):?>
					<?php $this->widget('zii.widgets.CBreadcrumbs', array(
						'links'=>$this->breadcrumbs,
					)); ?><!-- breadcrumbs -->
				<?php endif?>
			</div>
		</div>
	</div>
</div>
<div id="alert">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
					<div class="info alert alert-<?php print $key; ?>"><?php print $message; ?></div>
				<?php } ?>
				<?php Yii::app()->clientScript->registerScript('hideAlert',
						'$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
						CClientScript::POS_READY
				); ?>
			</div>
		</div>
	</div>
</div>
<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php echo $content; ?>
			</div>
		</div>
	</div>
</div>

<div class="clearfix">&nbsp;</div>

</body>
</html>
