<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="row">
	<div class="col-lg-3 col-md-3">
		<div class="sidebar-box" id="pengumuman-penting">
			<?php $referralImg = CHtml::image("images/pengumuman-penting.jpg", "#", array('id'=>'referral_img')); 
			echo CHtml::link($referralImg, array("page/read",'id'=>5)); ?>
		</div>
		<div class="sidebar-box" id="login-sim-kjp">
			<?php $referralImg = CHtml::image("images/login-sim-kjp.jpg", "#", array('id'=>'referral_img','class'=>'')); 
			echo CHtml::link($referralImg, array('site/login'), array('target'=>'_blank')); ?>
		</div>
		<div class="sidebar-box" id="panduan-operator-sekolah">
			<?php $referralImg = CHtml::image("images/panduan-operator-sekolah.jpg", "#", array('id'=>'referral_img')); 
			echo CHtml::link($referralImg, array("page/read",'id'=>8)); ?>
		</div>
		<div class="sidebar-box" id="format-usulan-kjp">
			<?php $referralImg = CHtml::image("images/format-usulan.jpg", "#", array('id'=>'referral_img')); 
			echo CHtml::link($referralImg, array("page/read",'id'=>9)); ?>
		</div>
		<div class="sidebar-box" id="hotline-service-kjp">
			<?php $referralImg = CHtml::image("images/hotline-service-kjp.jpg", "#", array('id'=>'referral_img')); 
			echo CHtml::link($referralImg, 'https://id.messenger.yahoo.com/web/', array('target'=>'_blank')); ?>
		</div>
		<div class="sidebar-box" id="daftar-siswa-penerima-kjp">
			<?php $referralImg = CHtml::image("images/daftar-siswa-kjp.jpg", "#", array('id'=>'referral_img')); 
			echo CHtml::link($referralImg, array("page/read",'id'=>6)); ?>
		</div>
		<div class="sidebar-box" id="tautan">
			<div id="image-tautan"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/tautan.png'); ?></div>
			<div id="text"><ul>
				<li><?php echo CHtml::link('Disdik Kab Tangerang','http://disdik.tangerangkab.go.id', array('target'=>'_blank')); ?></li>
				<li><?php echo CHtml::link('Pemkab Tangerang','http://tangerangkab.go.id/', array('target'=>'_blank')); ?></li>
				<li><?php echo CHtml::link('Berita Kab Tangerang','http://tangerangkab.go.id/', array('target'=>'_blank')); ?></li>
			</ul></div>
		</div>				<?php /*
		<div class="sidebar-box" id="berita">
		<!-- start feedwind code -->
		<script type="text/javascript">document.write('<script type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://' : 'http://') + 'feed.mikle.com/js/rssmikle.js"><' + '/script>');</script><script type="text/javascript">(function() {var params = {rssmikle_url: "tangerangkab.go.id/?cat=3/",rssmikle_frame_width: "221",rssmikle_frame_height: "300",rssmikle_target: "_blank",rssmikle_font: "Arial, Helvetica, sans-serif",rssmikle_font_size: "12",rssmikle_border: "off",responsive: "off",rssmikle_css_url: "",text_align: "left",text_align2: "left",corner: "off",scrollbar: "off",autoscroll: "on",scrolldirection: "up",scrollstep: "3",mcspeed: "20",sort: "New",rssmikle_title: "on",rssmikle_title_sentence: "Berita Terbaru Kabupaten Tangerang",rssmikle_title_link: "",rssmikle_title_bgcolor: "#FFF800",rssmikle_title_color: "#8200B8",rssmikle_title_bgimage: "",rssmikle_item_bgcolor: "#FFFFFF",rssmikle_item_bgimage: "",rssmikle_item_title_length: "55",rssmikle_item_title_color: "#8200B8",rssmikle_item_border_bottom: "on",rssmikle_item_description: "title_only",item_link: "off",rssmikle_item_description_length: "150",rssmikle_item_description_color: "#666666",rssmikle_item_date: "gl1",rssmikle_timezone: "Etc/GMT",datetime_format: "%b %e, %Y %l:%M:%S %p",rssmikle_item_description_tag: "off",rssmikle_item_description_image_scaling: "off",article_num: "15",rssmikle_item_podcast: "off",keyword_inc: "",keyword_exc: ""};feedwind_show_widget_iframe(params);})();</script><div style="font-size:10px; margin-top:15px; text-align:center; width:221;"><a href="http://feed.mikle.com/" target="_blank" style="color:#CCCCCC;">RSS Feed Widget</a><!--Please display the above link in your web page according to Terms of Service.--></div><!-- end feedwind code -->
		</div>		*/ ?>
	</div>
	<div class="col-lg-9 col-md-9">
		<div id="column2-content">
			<?php print $content; ?>
		</div>
	</div>
</div>
<?php $this->endContent(); ?>