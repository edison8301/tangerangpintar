<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<?php 	
			Yii::app()->clientScript->registerCoreScript('jquery');     
			Yii::app()->clientScript->registerCoreScript('jquery.ui'); 
	?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
	<div id="header">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div id="logo">
						<?php print CHtml::image(Yii::app()->baseUrl."/images/logo.png"); ?>
					</div>					
				</div>
			</div>
			
		</div>
	</div><!-- header -->
	<div id="mainmenu">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<ul>
						<li><a class="first" href="<?php print $this->createUrl('site/index'); ?>"><i class="glyphicon glyphicon-home"></i> HOME</a></li>
						<li><a href="<?php print $this->createUrl('page/read',array('id'=>1)); ?>">REKAP USULAN  </a></li>						<li><a href="<?php print $this->createUrl('page/read',array('id'=>6)); ?>">REKAP PENERIMA</a></li>
						<li><a href="<?php print $this->createUrl('page/read',array('id'=>2)); ?>">TAHAP PENGAJUAN USULAN </a></li>
						<li><a href="<?php print $this->createUrl('page/read',array('id'=>3)); ?>">LAPORAN PENGGUNAAN KPKT</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<?php if(Yii::app()->controller->id == 'site' AND Yii::app()->controller->action->id == 'index') { ?>
	<div id="slideshow">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php
						$this->widget('ext.slider.slider', array(
							'container'=>'slideshow',
							'width'=>930, 
							'height'=>270, 
							'timeout'=>5000,
							'infos'=>false,
							'constrainImage'=>true,
							'images'=>Slide::model()->getSlideImage()
							//'images'=>array('_slide01.jpg','_slide02.jpg'),
							//'defaultUrl'=>Yii::app()->request->hostInfo
							)
						);
					?>
				</div>
			</div>
		</div>
	</div><!-- slideshow -->
	<div id="slidespace">&nbsp;</div>
	<?php } ?>
	<div id="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
				<?php if(isset($this->breadcrumbs)):?>
					<?php $this->widget('zii.widgets.CBreadcrumbs', array(
						'links'=>$this->breadcrumbs,
					)); ?><!-- breadcrumbs -->
				<?php endif?>
				</div>
			</div>
		</div>
	</div>
<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div id="content-wrapper" style="">
					<?php echo $content; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div style="padding:10px;text-align:center;color:#053D49;font-family:OpenSans-Bold">
				Copyright &copy; <?php echo date('Y'); ?> Program Kartu Pintar Kabupaten Tangerang - Dinas Pendidikan Kabupaten Tangerang.
				</div>
			</div>
		</div>
	</div>		
</div><!-- footer -->
</body>
</html>
