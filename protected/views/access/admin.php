<?php
$this->breadcrumbs=array(
	'Access'=>array('index'),
	'Kelola',
);

//$this->menu=array(
//array('label'=>'List Data Nasabah','url'=>array('index')),
//array('label'=>'Create Data Nasabah','url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('data-access-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Data Access</h1>

<?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'link',
		'context'=>'primary',
		'url'=>array('access/create'),
		'label' => 'Tambah',
		'icon'=>'plus',
)); ?>

<?php /*
<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
*/ ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'data-access-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'id',
			'nama_controller',
			'nama_action',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>