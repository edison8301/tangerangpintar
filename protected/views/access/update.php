<?php
$this->breadcrumbs=array(
	'Accesse'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Access','url'=>array('index')),
	array('label'=>'Tambah Access','url'=>array('create')),
	array('label'=>'Lihat Access','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Access','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Access <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>