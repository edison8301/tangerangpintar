<?php
$this->breadcrumbs=array(
	'Access'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Access','url'=>array('index')),
array('label'=>'Tambah Access','url'=>array('create')),
array('label'=>'Perbaharui Access','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Access','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Access','url'=>array('admin')),
);
?>

<h1>Lihat Access #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama_controller',
		'nama_action',
),
)); ?>
