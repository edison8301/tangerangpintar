<?php
$this->breadcrumbs=array(
	'Agama'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Data Agama','url'=>array('index')),
array('label'=>'Tambah Data Agama','url'=>array('create')),
array('label'=>'Perbaharui Data Agama','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Data Agama','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Data Agama','url'=>array('admin')),
);
?>

<h1>Lihat Data Agama #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'agama',
),
)); ?>
