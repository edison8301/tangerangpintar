<?php
$this->breadcrumbs=array(
	'Agama'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Agama','url'=>array('index')),
	array('label'=>'Tambah Data Agama','url'=>array('create')),
	array('label'=>'Lihat Data Agama','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Agama','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Agama <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>