<?php
$this->breadcrumbs=array(
	'Artikel'=>array('admin'),
	'Kelola',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('post-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Kelola Berita</h1>


<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah Berita',
		'url'=>array('post/create')
)); ?>&nbsp;


<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'post-grid',
	'dataProvider'=>$model->search(),
	'type'=>'striped bordered',
	'filter'=>$model,
	'columns'=>array(
		'title',
		array(
			'class'=>'CDataColumn',
			'name'=>'thumbnail',
			'type'=>'raw',
			'value'=>'$data->getThumbnail(array("width"=>"180px"))'
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'post_category_id',
			'type'=>'raw',
			'value'=>'$data->post_category->title',
			'filter'=>CHtml::listData(PostCategory::model()->findAll(array('order'=>'title ASC')),'id','title')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'created_time',
			'type'=>'raw',
			'value'=>'$data->getCreatedTime()'
		),
		array(
			'class'=>'booster.widgets.TbButtonColumn',
		),
	),
)); ?>
