<?php
$this->breadcrumbs=array(
	'Posts'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Post','url'=>array('index')),
	array('label'=>'Create Post','url'=>array('create')),
	array('label'=>'Update Post','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Post','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Post','url'=>array('admin')),
);
?>

<div class="margin20" id="post-read">

	<h3 class="list-bottom"><?php print strtoupper($model->post_category->title); ?></h3>

	<hr>

	<h3 class='post-read-title'><?php print $model->title; ?></h3>

	<div class='post-read-created-time'>
		<?php print Bantu::getWaktuDibuat($model->created_time); ?>
	</div>

	<?php if($model->thumbnail!='') print CHtml::image(Yii::app()->request->baseUrl.'/uploads/post/'.$model->thumbnail); ?>

	<div>&nbsp;</div>

	<?php print $model->content; ?>
	
	<div>&nbsp;</div>
	
	<h4>Berita Lainnya</h4>
	<ul>
	<?php foreach($model->getOtherPost() as $data) { ?>
	<li><?php print CHtml::link($data->title,array('post/read','id'=>$data->id)); ?></li>
	<?php } ?>
	</ul>
</div>


