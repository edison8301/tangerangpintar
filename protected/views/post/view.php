<?php
$this->breadcrumbs=array(
	'Posts'=>array('index'),
	$model->title,
);

/*
$this->menu=array(
	array('label'=>'List Post','url'=>array('index')),
	array('label'=>'Create Post','url'=>array('create')),
	array('label'=>'Update Post','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Post','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Post','url'=>array('admin')),
);
*/
?>

<h1>Lihat Artikel</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'context'=>'primary',
		'icon'=>'pencil white',
		'url'=>array('/post/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Baca',
		'context'=>'primary',
		'icon'=>'search white',
		'url'=>array('/post/read','id'=>$model->id),
		'htmlOptions'=>array('target'=>'_blank')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/post/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola',
		'context'=>'primary',
		'icon'=>'list white',
		'url'=>array('/post/admin')
)); ?>


<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'type'=>'striped bordered',
	'attributes'=>array(
		'title',
		array(
			'label'=>'Kategori',
			'type'=>'raw',
			'value'=>$model->post_category->title
		),
		array(
			'label'=>'Konten',
			'type'=>'raw',
			'value'=>$model->content
		),
		array(
			'label'=>'Thumbnail',
			'type'=>'raw',
			'value'=>$model->thumbnail == '' ? '' : CHtml::image(Yii::app()->request->baseUrl.'/uploads/post/'.$model->thumbnail)
		),
	),
)); ?>

<h2>File Unduhan</h2>
<?php $this->widget('bootstrap.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/download/create','model'=>'Post','model_id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<table class='table'>
<tr>
	<th style="width:8%">No</th>
	<th>Judul</th>
	<th>Unduh</th>
	<th>Action</th>
</tr>
<?php $i=1; foreach($model->getDataDownload() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->title; ?></td>
	<td>
		<?php print CHtml::link('<i class="icon-download-alt"></i>',Yii::app()->request->baseUrl.'/uploads/download/'.$data->file,array('target'=>'_blank')); ?>
	</td>
	<td>
		<?php print CHtml::link('<i class="icon-pencil"></i>',array('download/update','id'=>$data->id)); ?>
		<?php print CHtml::link('<i class="icon-trash"></i>',array('download/hapus','id'=>$data->id)); ?>
	</td>
</tr>
<?php $i++; } ?>
</table>
