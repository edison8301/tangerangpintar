<?php
$this->breadcrumbs=array(
	'Role'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Role','url'=>array('index')),
	array('label'=>'Tambah Role','url'=>array('create')),
	array('label'=>'Lihat Role','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Role','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Role <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>