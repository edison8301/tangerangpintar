<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'role-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$role->search(),
		'filter'=>$role,
		'columns'=>array(
			array(
				'header'=>'Akses',
				'type'=>'raw',
				'value'=>'CHtml::link("<i class=\"icon-lock\"></i>",array("role/access","id"=>$data->id))'
			),
			'nama',
			array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>