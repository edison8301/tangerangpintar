<?php
$this->breadcrumbs=array(
	'Role'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Role','url'=>array('index')),
array('label'=>'Tambah Role','url'=>array('create')),
array('label'=>'Perbaharui Role','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Role','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Role','url'=>array('admin')),
);
?>

<h1>Lihat Role #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
