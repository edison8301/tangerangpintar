<?php
$this->breadcrumbs=array(
	'Penghasilan Per Tahun'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'Daftar Data Penghasilan Per Tahun','url'=>array('index')),
	array('label'=>'Tambah Data Penghasilan Per Tahun','url'=>array('create')),
	array('label'=>'Lihat Data Penghasilan Per Tahun','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Penghasilan Per Tahun','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Penghasilan Per Tahun <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>