<?php
$this->breadcrumbs=array(
	'Penghasilan Per Tahun'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Penghasilan Per Tahun','url'=>array('index')),
array('label'=>'Tambah Penghasilan Per Tahun','url'=>array('create')),
array('label'=>'Perbaharui Penghasilan Per Tahun','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Penghasilan Per Tahun','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Penghasilan Per Tahun','url'=>array('admin')),
);
?>

<h1>View PpTahun #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'pp_tahun',
),
)); ?>
