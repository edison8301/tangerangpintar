<?php
$this->breadcrumbs=array(
	'Sekolah'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Sekolah','url'=>array('index')),
	array('label'=>'Tambah Sekolah','url'=>array('create')),
	array('label'=>'Lihat Sekolah','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Sekolah','url'=>array('admin')),
	);
	?>

	<h1>Update Data Sekolah <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>