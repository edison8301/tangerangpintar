<?php
$this->breadcrumbs=array(
	'Sekolah'=>array('index'),
	'Kelola',
);

//$this->menu=array(
//array('label'=>'List Data Nasabah','url'=>array('index')),
//array('label'=>'Create Data Nasabah','url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('data-sekolah-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>DATA SEKOLAH</h1>

<?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'link',
		'context'=>'primary',
		'url'=>array('sekolah/create'),
		'label' => 'Tambah Data Sekolah',
		'icon'=>'plus',
)); ?>

<?php /*
<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
*/ ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'data-sekolah-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama_sekolah',
			array(
				'class'=>'CDataColumn',
				'name'=>'jenjang_sekolah_id',
				'header'=>'Jenjang Sekolah',
				'value'=>'$data->Sekolah->jenjang_sekolah',
				'filter'=>CHtml::listData(JenjangSekolah::model()->findAll(),'id','jenjang_sekolah')
			),
			'email',
			'alamat',
			'rt',
			'rw',
			'kelurahan',
			'kecamatan',
			'kota',
			'provinsi',
			'kode_pos',
			'telepon',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
