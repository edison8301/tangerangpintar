<?php
$this->breadcrumbs=array(
	'Sekolah'=>array('index'),
	$model->nama_sekolah,
);

$this->menu=array(
array('label'=>'Daftar Sekolah','url'=>array('index')),
array('label'=>'Tambah Sekolah','url'=>array('create')),
array('label'=>'Perbaharui Sekolah','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Sekolah','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Sekolah','url'=>array('admin')),
);
?>

<h1>Data <?php echo $model->nama_sekolah; ?></h1>

<?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'link',
		'context'=>'primary',
		'url'=>array('sekolah/update','id'=>$model->id),
		'label' => 'Sunting',
		'icon'=>'pencil',
)); ?>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'nama_sekolah',
			array(
				'label'=>'Jenjang Sekolah',
				'value'=>$model->Sekolah->jenjang_sekolah
			),
			'email',
			'alamat',
			'rt',
			'rw',
			'kelurahan',
			'kecamatan',
			'kota',
			'provinsi',
			'kode_pos',
			'telepon',
		),
)); ?>
