<?php
$this->breadcrumbs=array(
	'Sekolah'=>array('index'),
	'Tambah',
);

$this->menu=array(
array('label'=>'Daftar Sekolah','url'=>array('index')),
array('label'=>'Kelola Sekolah','url'=>array('admin')),
);
?>

<h1>Tambah Data Sekolah</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>