<?php
$this->breadcrumbs=array(
	'Sekolah'=>array('index'),
	'Profil',
);

?>

<h1>Profil Sekolah <?php print $model->nama_sekolah; ?></h1>

<p class="help-block">Kolom dengan <span class="required">*</span> wajib diisi.</p>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'sekolah-form',
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($user); ?>
	<?php echo $form->passwordFieldGroup($user,'password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
	
	<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Perbarui Password',
	)); ?>
	</div>


<?php $this->endWidget(); ?>


<hr>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'sekolah-form',
	'enableAjaxValidation'=>false,
)); ?>

	
	<?php echo $form->errorSummary($model); ?>

	<?php //echo $form->textFieldGroup($model,'nama_sekolah',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255,'readonly'=>'readonly')))); ?>

	<?php //echo $form->dropDownListGroup($model,'jenjang_sekolah_id',array('widgetOptions'=>array('data' =>CHtml::listData(JenjangSekolah::model()->findAll(),'id','jenjang_sekolah')))); ?>

	<?php echo $form->textFieldGroup($model,'email',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'alamat',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'rt',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>

	<?php echo $form->textFieldGroup($model,'rw',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>

	<?php echo $form->textFieldGroup($model,'kelurahan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'kecamatan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'kota',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'provinsi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'kode_pos',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>

	<?php echo $form->textFieldGroup($model,'telepon',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Perbarui Data Profil',
	)); ?>
	</div>

<?php $this->endWidget(); ?>
