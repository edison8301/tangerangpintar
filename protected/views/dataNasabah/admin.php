<?php
$this->breadcrumbs=array(
	'Data Nasabah'=>array('index'),
	'Kelola',
);

//$this->menu=array(
//array('label'=>'List Data Nasabah','url'=>array('index')),
//array('label'=>'Create Data Nasabah','url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('data-nasabah-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>MANAGEMENT DATA SISWA USULAN</h1>

<?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'link',
		'context'=>'success',
		'url'=>array('dataNasabah/create'),
		'label' => 'Tambah Data Siswa Usulan',
		'icon'=>'plus',
)); ?>
<div>&nbsp;</div>

<?php /*
<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
*/ ?>

<div style="overflow:auto">
<?php $this->widget('booster.widgets.TbTabs',array(
			'type' => 'tabs', // 'tabs' or 'pills'
			'tabs' => array(
				array(
					'label' => 'Persyaratan Belum Lengkap', 
					'active'=>true,
					'content' => $this->renderPartial('_usulan_belum_lengkap_admin',array('model'=>$model),$this)
				),
				array(
					'label' => 'Persyaratan Sudah Lengkap',
					'encodeLabel'=>false,
					'content' => $this->renderPartial('_usulan_lengkap_admin',array('model'=>$model),$this)
				),
				
			),
	)); ?>
</div>
