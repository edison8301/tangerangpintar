<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'link',
		'context'=>'primary',
		'url'=>array('dataNasabah/exportExcel','persyaratan'=>0),
		'label' => 'Export Data Siswa Usulan Dengan Persyaratan Belum Lengkap',
		'icon'=>'download-alt',
)); ?>

<div style="overflow:auto">
<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'data-nasabah-belum-lengkap-admin-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->searchSyaratBelumLengkap(),
		'filter'=>$model,
		'columns'=>array(
				array(
				'class'=>'booster.widgets.TbButtonColumn',
				'header'=>'Lengkap',
				'template' => '{syarat}',
				'buttons'=>array(
					'syarat' => array(
                                'label'=>'Tekan Tombol Jika Syarat Sudah Lengkap', // text label of the button
                                'url'=>"CHtml::normalizeUrl(array('persyaratanLengkap', 'id'=>\$data->id))",
                                'icon'=>'ok-circle',  // image URL of the button. If not set or false, a text link is used
                                'options' => array('class'=>'syarat'), // HTML options for the button
                        ),
					),
				),
				array(
					'class'=>'booster.widgets.TbButtonColumn',
					'template' => '{view} {update} {delete}',
					'buttons'=>array(
						'update' => array('url'=>'Yii::app()->createUrl("dataNasabah/update", array("id"=>$data->id))',),
						'view' => array('url'=>'Yii::app()->createUrl("dataNasabah/view", array("id"=>$data->id))',),
						'delete' => array('url'=>'Yii::app()->createUrl("dataNasabah/delete", array("id"=>$data->id))',),
						),
					),
				/*
				array(
					'class'=>'CDataColumn',
					'name'=>'id',
					'type'=>'html',
					'header'=>'Lengkap',
					'value'=>'CHtml::link("<i class=\"glyphicon glyphicon-ok-circle\"></i> Lengkap",array("persyaratanLengkap","id"=>$data->id),array("data-toggle"=>"tooltip","title"=>"Tekan Tombol Jika Syarat Sudah Lengkap"))',
					//'value'=>'Yii::app()->controller->widget("booster.widgets.TbButton",array("buttonType"=>"link","context"=>"primary","icon"=>"ok-circle","url"=>array("persyaratanLengkap","id"=>$data->id)))'
				),
				*/
				'tahun',
				array(
				'class'=>'CDataColumn',
				'name'=>'id_sekolah',
				'header'=>'Sekolah',
				'value'=>'$data->getRelationField("Sekolah","nama_sekolah")',
				'filter'=>CHtml::listData(Sekolah::model()->findAll(),'id','nama_sekolah')
			),
				'nama',
				array(
					'class'=>'CDataColumn',
					'name'=>'persyaratan',
					'header'=>'Persyaratan',
					'value'=>'$data->persyaratan($data->persyaratan)',
					'filter'=>array('0'=>'Belum Lengkap','1'=>'Lengkap')
				),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_jk',
				'header'=>'Jenis Kelamin',
				'value'=>'$data->getRelationField("Jk","jenis_kelamin")',
				'filter'=>CHtml::listData(JenisKelamin::model()->findAll(),'id','jenis_kelamin')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_kebangsaan',
				'header'=>'Kebangsaan',
				'value'=>'$data->getRelationField("Kebangsaan","kebangsaan")',
				'filter'=>CHtml::listData(Kebangsaan::model()->findAll(),'id','kebangsaan')
			),
			'tempat_lahir',
			'tanggal_lahir',
			'no_identitas',
			'mb_identitas',
			'npwp',
			'nama_ibu',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_sk',
				'header'=>'Status Kawin',
				'value'=>'$data->getRelationField("StatusKawin","status_kawin")',
				'filter'=>CHtml::listData(StatusKawin::model()->findAll(),'id','status_kawin')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_agama',
				'header'=>'Agama',
				'value'=>'$data->getRelationField("Agama","agama")',
				'filter'=>CHtml::listData(Agama::model()->findAll(),'id','agama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pendidikan',
				'header'=>'Pendidikan',
				'value'=>'$data->getRelationField("Pendidikan","pendidikan")',
				'filter'=>CHtml::listData(Pendidikan::model()->findAll(),'id','pendidikan')
			),
			'alamat',
			'rt',
			'rw',
			'kelurahan',
			'kecamatan',
			'kota',
			'propinsi',
			'kode_pos',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_stt',
				'header'=>'Status Tempat Tinggal',
				'value'=>'$data->getRelationField("Stt","status_ttinggal")',
				'filter'=>CHtml::listData(StatusTtinggal::model()->findAll(),'id','status_ttinggal')
			),
			'no_hp',
			'no_rumah',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_asurat',
				'header'=>'Alamat Surat',
				'value'=>'$data->getRelationField("AlamatSurat","alamat_surat")',
				'filter'=>CHtml::listData(AlamatSurat::model()->findAll(),'id','alamat_surat')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_tialamat',
				'header'=>'Tipe Alamat',
				'value'=>'$data->getRelationField("TipeAlamat","tipe_alamat")',
				'filter'=>CHtml::listData(TipeAlamat::model()->findAll(),'id','tipe_alamat')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pekerjaan',
				'header'=>'Pekerjaan',
				'value'=>'$data->getRelationField("Pekerjaan","pekerjaan")',
				'filter'=>CHtml::listData(Pekerjaan::model()->findAll(),'id','pekerjaan')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'kode_profesi',
				'header'=>'Kode Profesi',
				'value'=>'$data->getRelationField("KodeProfesi","kode_profesi")',
				'filter'=>CHtml::listData(KodeProfesi::model()->findAll(),'id','kode_profesi')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'status_pekerjaan',
				'header'=>'Status Pekerjaan',
				'value'=>'$data->getRelationField("StatusPekerjaan","status_pekerjaan")',
				'filter'=>CHtml::listData(StatusPekerjaan::model()->findAll(),'id','status_pekerjaan')
			),
			'nama_instansi',
			'alamat_instansi',
			'rt_dp',
			'rw_dp',
			'kelurahan_dp',
			'kecamatan_dp',
			'kota_dp',
			'propinsi_dp',
			'kodepos_dp',
			'notelp_dp',
			'suami_istri',
			'nama_pydd',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_hubungan',
				'header'=>'Hubungan',
				'value'=>'$data->getRelationField("Hubungan","hubungan")',
				'filter'=>CHtml::listData(Hubungan::model()->findAll(),'id','hubungan')
			),
			'alamat_pydd',
			'kota_pydd',
			'propinsi_pydd',
			'notelp_pydd',
			array(
				'class'=>'CDataColumn',
				'name'=>'sumber_dana',
				'header'=>'Sumber Dana',
				'value'=>'$data->getRelationField("SumberDana","sumber_dana")',
				'filter'=>CHtml::listData(SumberDana::model()->findAll(),'id','sumber_dana')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tp_dana',
				'header'=>'Tujuan Penggunaan Dana',
				'value'=>'$data->getRelationField("Tpd","tujuan_pdana")',
				'filter'=>CHtml::listData(TujuanPdana::model()->findAll(),'id','tujuan_pdana')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'pp_tahun',
				'header'=>'Penghasilan Per Tahun',
				'value'=>'$data->getRelationField("Ppt","pp_tahun")',
				'filter'=>CHtml::listData(PpTahun::model()->findAll(),'id','pp_tahun')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'ptp_tahun',
				'header'=>'Penghasilan Tambahan Per Tahun',
				'value'=>'$data->getRelationField("Ptp","ptp_tahun")',
				'filter'=>CHtml::listData(PtpTahun::model()->findAll(),'id','ptp_tahun')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'max_spbulan',
				'header'=>'Max Setoran Per Bulan',
				'value'=>'$data->getRelationField("Maxspb","max_spbulan")',
				'filter'=>CHtml::listData(MaxSpbulan::model()->findAll(),'id','max_spbulan')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'max_tpbulan',
				'header'=>'Max Tarikan Per Bulan',
				'value'=>'$data->getRelationField("Maxtpb","max_tpbulan")',
				'filter'=>CHtml::listData(MaxTpbulan::model()->findAll(),'id','max_tpbulan')
			),
			'kjp',
			'nisn',
			),
	)); ?>
</div>