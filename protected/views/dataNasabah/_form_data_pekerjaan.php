
	<div>&nbsp;</div>
	
	<?php echo $form->dropDownListGroup($model,'id_pekerjaan',array('widgetOptions'=>array('data' => CHtml::listData(Pekerjaan::model()->findAll(),'id','pekerjaan')))); ?>

	<?php echo $form->dropDownListGroup($model,'kode_profesi',array('widgetOptions'=>array('data' => CHtml::listData(KodeProfesi::model()->findAll(),'id','kode_profesi')))); ?>

	<?php echo $form->dropDownListGroup($model,'status_pekerjaan',array('widgetOptions'=>array('data' => CHtml::listData(StatusPekerjaan::model()->findAll(),'id','status_pekerjaan')))); ?>

	<?php echo $form->textFieldGroup($model,'nama_instansi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'alamat_instansi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'rt_dp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>3)))); ?>

	<?php echo $form->textFieldGroup($model,'rw_dp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>3)))); ?>

	<?php echo $form->textFieldGroup($model,'kelurahan_dp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'kecamatan_dp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'kota_dp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'propinsi_dp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'kodepos_dp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>5)))); ?>

	<?php echo $form->textFieldGroup($model,'notelp_dp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>13)))); ?>

	<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>'Simpan Semua Data',
			'icon'=>'floppy-disk',
		)); ?>
	</div>

