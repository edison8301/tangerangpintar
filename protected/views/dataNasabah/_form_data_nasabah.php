
	<div>&nbsp;</div>
	
	<?php if(Yii::app()->user->getState('role_id')==1) { ?>
		<?php echo $form->dropDownListGroup($model,'id_sekolah',array('widgetOptions'=>array('data'=>CHtml::listData(Sekolah::model()->findAll(),'id','nama_sekolah')))); ?>
	<?php } else { ?>
		<?php echo $form->dropDownListGroup($model,'id_sekolah',array('widgetOptions'=>array('data'=>CHtml::listData(Sekolah::model()->findAll(),'id','nama_sekolah'),'htmlOptions'=>array('options'=>array(Yii::app()->user->getState('id_sekolah')=>array('selected'=>true)),'disabled'=>true)))); ?>
	<?php } ?>
	
	<?php echo $form->textFieldGroup($model,'nama',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->dropDownListGroup($model,'id_jk',array('widgetOptions'=>array('data' =>CHtml::listData(JenisKelamin::model()->findAll(),'id','jenis_kelamin')))); ?>

	<?php echo $form->dropDownListGroup($model,'id_kebangsaan',array('widgetOptions'=>array('data' => CHtml::listData(Kebangsaan::model()->findAll(),'id','kebangsaan')))); ?>

	<?php echo $form->textFieldGroup($model,'tempat_lahir',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>25)))); ?>

	<?php echo $form->textFieldGroup($model,'tanggal_lahir',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8)))); ?>

	<?php echo $form->textFieldGroup($model,'no_identitas',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>16)))); ?>

	<?php echo $form->textFieldGroup($model,'mb_identitas',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8)))); ?>

	<?php echo $form->textFieldGroup($model,'npwp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>16)))); ?>

	<?php echo $form->textFieldGroup($model,'nama_ibu',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->dropDownListGroup($model,'id_sk',array('widgetOptions'=>array('data' => CHtml::listData(StatusKawin::model()->findAll(),'id','status_kawin')))); ?>

	<?php echo $form->dropDownListGroup($model,'id_agama',array('widgetOptions'=>array('data' => CHtml::listData(Agama::model()->findAll(),'id','agama')))); ?>

	<?php echo $form->dropDownListGroup($model,'id_pendidikan',array('widgetOptions'=>array('data' => CHtml::listData(Pendidikan::model()->findAll(),'id','pendidikan')))); ?>

	<?php echo $form->textFieldGroup($model,'alamat',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>75)))); ?>

	<?php echo $form->textFieldGroup($model,'rt',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>3)))); ?>

	<?php echo $form->textFieldGroup($model,'rw',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>3)))); ?>

	<?php echo $form->textFieldGroup($model,'kelurahan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'kecamatan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'kota',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'propinsi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'kode_pos',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>5)))); ?>

	<?php echo $form->dropDownListGroup($model,'id_stt',array('widgetOptions'=>array('data' => CHtml::listData(StatusTtinggal::model()->findAll(),'id','status_ttinggal')))); ?>

	<?php echo $form->textFieldGroup($model,'no_hp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>13)))); ?>

	<?php echo $form->textFieldGroup($model,'no_rumah',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>13)))); ?>

	<?php echo $form->dropDownListGroup($model,'id_asurat',array('widgetOptions'=>array('data' => CHtml::listData(AlamatSurat::model()->findAll(),'id','alamat_surat')))); ?>

	<?php echo $form->dropDownListGroup($model,'id_tialamat',array('widgetOptions'=>array('data' => CHtml::listData(TipeAlamat::model()->findAll(),'id','tipe_alamat')))); ?>
	
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>'Simpan Semua Data',
			'icon'=>'floppy-disk',
		)); ?>
	</div>

