<?php
$this->breadcrumbs=array(
	'Data Nasabah'=>array('admin'),
	'Perbaharui',
);

//$this->menu=array(
//array('label'=>'List DataNasabah','url'=>array('index')),
//array('label'=>'Manage DataNasabah','url'=>array('admin')),
//);
?>

<h1>Perbaharui Data Nasabah</h1>

<p class="help-block">Kolom dengan tanda <span class="required">*</span> wajib diisi.</p>


<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'data-nasabah-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php $this->widget('booster.widgets.TbTabs',array(
        'type' => 'tabs', // 'tabs' or 'pills'
        'tabs' => array(
            array('label' => 'Data Nasabah','content' => $this->renderPartial('_form_data_nasabah',array('model'=>$model,'form'=>$form),$this),'active' => true),
            array('label' => 'Data Pekerjaan / Sekolah','content' => $this->renderPartial('_form_data_pekerjaan',array('model'=>$model,'form'=>$form),$this)),
			array('label' => 'Pihak Yang Dapat Dihubungi Dalam Keadaan Darurat','content' => $this->renderPartial('_form_data_pydd',array('model'=>$model,'form'=>$form),$this)),
            array('label' => 'Data Keuangan','content' => $this->renderPartial('_form_data_keuangan',array('model'=>$model,'form'=>$form),$this)),
            
			//array('label' => 'Profile', 'content' => $this->renderPartial('),
            //array('label' => 'Messages', 'content' => 'Messages Content'),
        ),
)); ?>

<?php $this->endWidget(); ?>
