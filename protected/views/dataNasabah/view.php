<?php
$this->breadcrumbs=array(
	'Data Nasabah'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Data Nasabah','url'=>array('index')),
array('label'=>'Tambah Data Nasabah','url'=>array('create')),
array('label'=>'Perbaharui Data Nasabah','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Data Nasabah','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Data Nasabah','url'=>array('admin')),
);
?>

<h1>Lihat Data Nasabah <?php echo $model->nama; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'id',
			'nama',
			array(
				'label'=>'Jenis Kelamin',
				'value'=>$model->getRelationField("Jk","jenis_kelamin")
			),
			array(
				'label'=>'Kebangsaan',
				'value'=>$model->getRelationField("Kebangsaan","kebangsaan")
			),
			'tempat_lahir',
			'tanggal_lahir',
			'no_identitas',
			'mb_identitas',
			'npwp',
			'nama_ibu',
			array(
				'label'=>'Status Kawin',
				'value'=>$model->getRelationField("StatusKawin","status_kawin")
			),
			array(
				'label'=>'Agama',
				'value'=>$model->getRelationField("Agama","agama")
			),
			array(
				'label'=>'Pendidikan',
				'value'=>$model->getRelationField("Pendidikan","pendidikan")
			),
			'alamat',
			'rt',
			'rw',
			'kelurahan',
			'kecamatan',
			'kota',
			'propinsi',
			'kode_pos',
			array(
				'label'=>'Status Tempat Tinggal',
				'value'=>$model->getRelationField("Stt","status_ttinggal")
			),
			'no_hp',
			'no_rumah',
			array(
				'label'=>'Alamat Surat',
				'value'=>$model->getRelationField("AlamatSurat","alamat_surat")
			),
			array(
				'label'=>'Tipe Alamat',
				'value'=>$model->getRelationField("TipeAlamat","tipe_alamat")
			),
			array(
				'label'=>'Pekerjaan',
				'value'=>$model->getRelationField("Pekerjaan","pekerjaan")
			),
			array(
				'label'=>'Kode Profesi',
				'value'=>$model->getRelationField("KodeProfesi","kode_profesi")
			),
			array(
				'label'=>'Status Pekerjaan',
				'value'=>$model->getRelationField("StatusPekerjaan","status_pekerjaan")
			),
			'nama_instansi',
			'alamat_instansi',
			'rt_dp',
			'rw_dp',
			'kelurahan_dp',
			'kecamatan_dp',
			'kota_dp',
			'propinsi_dp',
			'kodepos_dp',
			'notelp_dp',
			'suami_istri',
			'nama_pydd',
			array(
				'label'=>'Hubungan',
				'value'=>$model->getRelationField("Hubungan","hubungan")
			),
			'alamat_pydd',
			'kota_pydd',
			'propinsi_pydd',
			'notelp_pydd',
			array(
				'label'=>'Sumber Dana',
				'value'=>$model->getRelationField("SumberDana","sumber_dana")
			),
			array(
				'label'=>'Tujuan Penggunaan Dana',
				'value'=>$model->getRelationField("Tpd","tujuan_pdana")
			),
			array(
				'label'=>'Penghasilan Per Tahun',
				'value'=>$model->getRelationField("Ppt","pp_tahun")
			),
			array(
				'label'=>'Penghasilan Tambahan Per Tahun',
				'value'=>$model->getRelationField("Ptp","ptp_tahun")
			),
			array(
				'label'=>'Max Setoran Per Bulan',
				'value'=>$model->getRelationField("Maxspb","max_spbulan")
			),
			array(
				'label'=>'Max Tarikan Per Bulan',
				'value'=>$model->getRelationField("Maxtpb","max_tpbulan")
			),
			'kjp',
			'nisn',
),	
)); ?>
