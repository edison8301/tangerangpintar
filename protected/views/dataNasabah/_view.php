<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jk')); ?>:</b>
	<?php echo CHtml::encode($data->id_jk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kebangsaan')); ?>:</b>
	<?php echo CHtml::encode($data->id_kebangsaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_identitas')); ?>:</b>
	<?php echo CHtml::encode($data->no_identitas); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mb_identitas')); ?>:</b>
	<?php echo CHtml::encode($data->mb_identitas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('npwp')); ?>:</b>
	<?php echo CHtml::encode($data->npwp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_ibu')); ?>:</b>
	<?php echo CHtml::encode($data->nama_ibu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_sk')); ?>:</b>
	<?php echo CHtml::encode($data->id_sk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_agama')); ?>:</b>
	<?php echo CHtml::encode($data->id_agama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pendidikan')); ?>:</b>
	<?php echo CHtml::encode($data->id_pendidikan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rt')); ?>:</b>
	<?php echo CHtml::encode($data->rt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rw')); ?>:</b>
	<?php echo CHtml::encode($data->rw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelurahan')); ?>:</b>
	<?php echo CHtml::encode($data->kelurahan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kecamatan')); ?>:</b>
	<?php echo CHtml::encode($data->kecamatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kota')); ?>:</b>
	<?php echo CHtml::encode($data->kota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('propinsi')); ?>:</b>
	<?php echo CHtml::encode($data->propinsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_pos')); ?>:</b>
	<?php echo CHtml::encode($data->kode_pos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_stt')); ?>:</b>
	<?php echo CHtml::encode($data->id_stt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_hp')); ?>:</b>
	<?php echo CHtml::encode($data->no_hp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_rumah')); ?>:</b>
	<?php echo CHtml::encode($data->no_rumah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_asurat')); ?>:</b>
	<?php echo CHtml::encode($data->id_asurat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tialamat')); ?>:</b>
	<?php echo CHtml::encode($data->id_tialamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->id_pekerjaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_profesi')); ?>:</b>
	<?php echo CHtml::encode($data->kode_profesi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->status_pekerjaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_instansi')); ?>:</b>
	<?php echo CHtml::encode($data->nama_instansi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_instansi')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_instansi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rt_dp')); ?>:</b>
	<?php echo CHtml::encode($data->rt_dp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rw_dp')); ?>:</b>
	<?php echo CHtml::encode($data->rw_dp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelurahan_dp')); ?>:</b>
	<?php echo CHtml::encode($data->kelurahan_dp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kecamatan_dp')); ?>:</b>
	<?php echo CHtml::encode($data->kecamatan_dp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kota_dp')); ?>:</b>
	<?php echo CHtml::encode($data->kota_dp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('propinsi_dp')); ?>:</b>
	<?php echo CHtml::encode($data->propinsi_dp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodepos_dp')); ?>:</b>
	<?php echo CHtml::encode($data->kodepos_dp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notelp_dp')); ?>:</b>
	<?php echo CHtml::encode($data->notelp_dp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('suami_istri')); ?>:</b>
	<?php echo CHtml::encode($data->suami_istri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_pydd')); ?>:</b>
	<?php echo CHtml::encode($data->nama_pydd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_hubungan')); ?>:</b>
	<?php echo CHtml::encode($data->id_hubungan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_pydd')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_pydd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kota_pydd')); ?>:</b>
	<?php echo CHtml::encode($data->kota_pydd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('propinsi_pydd')); ?>:</b>
	<?php echo CHtml::encode($data->propinsi_pydd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notelp_pydd')); ?>:</b>
	<?php echo CHtml::encode($data->notelp_pydd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sumber_dana')); ?>:</b>
	<?php echo CHtml::encode($data->sumber_dana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tp_dana')); ?>:</b>
	<?php echo CHtml::encode($data->tp_dana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pp_tahun')); ?>:</b>
	<?php echo CHtml::encode($data->pp_tahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ptp_tahun')); ?>:</b>
	<?php echo CHtml::encode($data->ptp_tahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_spbulan')); ?>:</b>
	<?php echo CHtml::encode($data->max_spbulan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_tpbulan')); ?>:</b>
	<?php echo CHtml::encode($data->max_tpbulan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kjp')); ?>:</b>
	<?php echo CHtml::encode($data->kjp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nisn')); ?>:</b>
	<?php echo CHtml::encode($data->nisn); ?>
	<br />

	*/ ?>

</div>