	
	<div>&nbsp;</div>
	
	<?php echo $form->textFieldGroup($model,'suami_istri',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>15)))); ?>

	<?php echo $form->textFieldGroup($model,'nama_pydd',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->dropDownListGroup($model,'id_hubungan',array('widgetOptions'=>array('data' => CHtml::listData(Hubungan::model()->findAll(),'id','hubungan')))); ?>

	<?php echo $form->textFieldGroup($model,'alamat_pydd',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>75)))); ?>

	<?php echo $form->textFieldGroup($model,'kota_pydd',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>

	<?php echo $form->textFieldGroup($model,'propinsi_pydd',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>

	<?php echo $form->textFieldGroup($model,'notelp_pydd',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>13)))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>'Simpan Semua Data',
			'icon'=>'floppy-disk',
		)); ?>
</div>

