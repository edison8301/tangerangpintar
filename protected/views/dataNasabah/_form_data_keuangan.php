
	<div>&nbsp;</div>
	
	<?php echo $form->dropDownListGroup($model,'sumber_dana',array('widgetOptions'=>array('data' => CHtml::listData(SumberDana::model()->findAll(),'id','sumber_dana')))); ?>

	<?php echo $form->dropDownListGroup($model,'tp_dana',array('widgetOptions'=>array('data' => CHtml::listData(TujuanPdana::model()->findAll(),'id','tujuan_pdana')))); ?>

	<?php echo $form->dropDownListGroup($model,'pp_tahun',array('widgetOptions'=>array('data' => CHtml::listData(PpTahun::model()->findAll(),'id','pp_tahun')))); ?>

	<?php echo $form->dropDownListGroup($model,'ptp_tahun',array('widgetOptions'=>array('data' => CHtml::listData(PtpTahun::model()->findAll(),'id','ptp_tahun')))); ?>

	<?php echo $form->dropDownListGroup($model,'max_spbulan',array('widgetOptions'=>array('data' => CHtml::listData(MaxSpbulan::model()->findAll(),'id','max_spbulan')))); ?>

	<?php echo $form->dropDownListGroup($model,'max_tpbulan',array('widgetOptions'=>array('data' => CHtml::listData(MaxTpbulan::model()->findAll(),'id','max_tpbulan')))); ?>

	<?php echo $form->textFieldGroup($model,'kjp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>25)))); ?>

	<?php echo $form->textFieldGroup($model,'nisn',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>

	<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>'Simpan Semua Data',
			'icon'=>'floppy-disk',
		)); ?>
	</div>

