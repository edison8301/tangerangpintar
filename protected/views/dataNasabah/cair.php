<?php
$this->breadcrumbs=array(
	'Data Nasabah'=>array('index'),
	'Kelola',
);

//$this->menu=array(
//array('label'=>'List Data Nasabah','url'=>array('index')),
//array('label'=>'Create Data Nasabah','url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('data-nasabah-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>PENCAIRAN DATA KPKT</h1>


<?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'link',
		'context'=>'primary',
		'url'=>array('dataNasabah/exportExcel','cair'=>1),
		'label' => 'Export Data Siswa Penerima',
		'icon'=>'download-alt',
)); ?>
<div style="overflow:auto">
<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'data-nasabah-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->searchCair(),
		'filter'=>$model,
		'columns'=>array(
				'id',
				'no_rekening',
				'nama',
				'nominal',
				'jumlah_bulan',
				'jumlah_nominal',
				array(
				'class'=>'CDataColumn',
				'name'=>'id_sekolah',
				'header'=>'Sekolah',
				'value'=>'$data->Sekolah->nama_sekolah',
				'filter'=>CHtml::listData(Sekolah::model()->findAll(),'id','nama_sekolah')
			),
				'alamat_sekolah',
				'bank',
				'kelas',
		),
)); ?>
</div>