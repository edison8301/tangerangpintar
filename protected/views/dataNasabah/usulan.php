<?php
$this->breadcrumbs=array(
	'Data Nasabah'=>array('index'),
	'Kelola',
);

//$this->menu=array(
//array('label'=>'List Data Nasabah','url'=>array('index')),
//array('label'=>'Create Data Nasabah','url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('data-nasabah-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>MANAGEMENT DATA SISWA USULAN</h1>

<?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'link',
		'context'=>'success',
		'url'=>array('dataNasabah/create'),
		'label' => 'Tambah Data Siswa Usulan',
		'icon'=>'plus',
)); ?>


<div>&nbsp;</div>

<div style="overflow:auto">
	<?php $this->widget('booster.widgets.TbTabs',array(
			'type' => 'tabs', // 'tabs' or 'pills'
			'tabs' => array(
				array(
					'label' => 'Persyaratan Belum Lengkap', 
					'active'=>true,
					'content' => $this->renderPartial('_usulan_belum_lengkap',array('model'=>$model),$this)
				),
				array(
					'label' => 'Persyaratan Sudah Lengkap',
					'content' => $this->renderPartial('_usulan_lengkap',array('model'=>$model),$this)
				),
				
			),
	)); ?>
</div>