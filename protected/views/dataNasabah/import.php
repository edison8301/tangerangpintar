<h1>Import Data Nasabah</h1>

<p>Silahkan gunakan form di bawah ini untuk mengupload file terlebih dahulu

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'product-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

<?php echo $form->fileFieldGroup($model,'file',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'submit',
		'label'=>'Upload',
		'icon'=>'open',
		'context'=>'primary',
	)); ?>
</div>



<?php $this->endWidget(); ?>