<?php
$this->breadcrumbs=array(
	'Kebangsaan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Kebangsaan','url'=>array('index')),
	array('label'=>'Tambah Data Kebangsaan','url'=>array('create')),
	array('label'=>'Lihat Data Kebangsaan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Kebangsaan','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Kebangsaan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>