<?php
$this->breadcrumbs=array(
	'Kebangsaans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Kebangsaan','url'=>array('index')),
array('label'=>'Tambah Kebangsaan','url'=>array('create')),
array('label'=>'Perbaharui Kebangsaan','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Kebangsaan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Kebangsaan','url'=>array('admin')),
);
?>

<h1>Lihat Data Kebangsaan #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'kebangsaan',
),
)); ?>
