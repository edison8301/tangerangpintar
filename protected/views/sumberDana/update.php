<?php
$this->breadcrumbs=array(
	'Sumber Dana'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Sumber Dana','url'=>array('index')),
	array('label'=>'Tambah Data Sumber Dana','url'=>array('create')),
	array('label'=>'Lihat Data Sumber Dana','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Sumber Dana','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Sumber Dana <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>