<?php
$this->breadcrumbs=array(
	'Sumber Dana'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar Sumber Dana','url'=>array('index')),
array('label'=>'Tambah Sumber Dana','url'=>array('create')),
array('label'=>'Perbaharui Sumber Dana','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Sumber Dana','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Sumber Dana','url'=>array('admin')),
);
?>

<h1>Lihat Sumber Dana #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'sumber_dana',
),
)); ?>
