<?php
$this->breadcrumbs=array(
	'Status Kawin'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar StatusKawin','url'=>array('index')),
array('label'=>'Tambah StatusKawin','url'=>array('create')),
array('label'=>'Perbaharui StatusKawin','url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus StatusKawin','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola StatusKawin','url'=>array('admin')),
);
?>

<h1>Lihat Status Kawin #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'status_kawin',
),
)); ?>
