<?php
$this->breadcrumbs=array(
	'Status Kawin'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Status Kawin','url'=>array('index')),
	array('label'=>'Tambah Data Status Kawin','url'=>array('create')),
	array('label'=>'Lihat Data Status Kawin','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Status Kawin','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Status Kawin <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>