<?php
$this->breadcrumbs=array(
	'Jenis Kelamins'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Daftar JenisKelamin','url'=>array('index')),
array('label'=>'Tambah Data Jenis Kelamin','url'=>array('create')),
array('label'=>'Update Data Jenis Kelamin','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Data Jenis Kelamin','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Data Jenis Kelamin','url'=>array('admin')),
);
?>

<h1>Lihat Data Jenis Kelamin #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'jenis_kelamin',
),
)); ?>
