<?php
$this->breadcrumbs=array(
	'Jenis Kelamin'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Perbaharui',
);

	$this->menu=array(
	array('label'=>'Daftar Data Jenis Kelamin','url'=>array('index')),
	array('label'=>'Tambah Data Jenis Kelamin','url'=>array('create')),
	array('label'=>'Lihat Data Jenis Kelamin','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kelola Data Jenis Kelamin','url'=>array('admin')),
	);
	?>

	<h1>Perbaharui Data Jenis Kelamin <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>